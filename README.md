# 面试题

### [Android面试题](Android面试题.md)
### [Flutter面试题](Flutter面试题.md)
### [Dart面试题](Dart面试题.md)

### 相关技能
- 熟练掌握Java、Flutter（Dart）开发语言
- 了解Dart空安全（null safety）相关知识
- 熟悉Android(App) + Flutter(Module)混合应用开发
- 熟练掌握 MVC、MVP、MVVM等架构模式进行开发
- 熟练掌握 OkHttp、Retrofit、RxJava网络框架（Android）
- 熟练掌握 Dio、Retrofit网络框架（Flutter）
- 熟悉Handler工作原理（Android）
- 深入理解Activity、Fragment生命周期（Android）
- 熟悉StatefulWidget生命周期（Flutter）
- 熟悉Flutter状态管理框架（Provider、Riverpod）
- 熟练掌握Glide，了解Glide三层缓存原理（Android）
- 熟练掌握CachedNetworkImage（Flutter）
- 熟练掌握Android的三种动画：帧动画、View动画（补间动画）、属性动画（Android）
- 熟练使用Dart Json泛型（Flutter）
- 使用go_router进行路由管理（Flutter）
- 熟练使用Flutter布局Rom，Column、ListView、GridView等（Flutter）
- 熟悉TabLayout + ViewPager2（Android）
- 使用AndroidStudio的Profiler优化，查看内存泄露
- 掌握Android/Flutter组件化开发