# Flutter面试题

### Hot Reload，Hot Restart的区别和原理
-  **区别**  
修改了状态相关的代码则需要hot restart，否则只需要 hot reload即可
-  **Hot Reload原理**  
Flutter 提供的两种编译模式中，AOT 是静态编译，即编译成设备可直接执行的二进制码；而 JIT 则是动态编译，即将 Dart 代码编译成中间代码（Script Snapshot），在运行时设备需要 Dart VM 解释执行。
而热重载之所以只能在 Debug 模式下使用，是因为 Debug 模式下，Flutter 采用的是 JIT 动态编译（而 Release 模式下采用的是 AOT 静态编译）。JIT 编译器将 Dart 代码编译成可以运行在 Dart VM 上的 Dart Kernel，而 Dart Kernel 是可以动态更新的，这就实现了代码的实时更新功能，
总体来说，完成热重载的可以分为扫描工程改动、增量编译、推送更新、代码合并、Widget 重建 5 个步骤。  
    1. 这里是列表文本工程改动。热重载模块会逐一扫描工程中的文件，检查是否有新增、删除或者改动，直到找到在上次编译之后，发生变化的 Dart 代码。
    2. 增量编译。热重载模块会将发生变化的 Dart 代码，通过编译转化为增量的 Dart Kernel 文件。
    3. 推送更新。热重载模块将增量的 Dart Kernel 文件通过 HTTP 端口，发送给正在移动设备上运行的 Dart VM。
    4. 代码合并。Dart VM 会将收到的增量 Dart Kernel 文件，与原有的 Dart Kernel 文件进行合并，然后重新加载新的Dart Kernel 文件。
    5. Widget 重建。在确认 Dart VM 资源加载成功后，Flutter 会将其 UI 线程重置，通知 Flutter Framework 重建 Widget。
-  **不支持 Hot Reload 的场景**  
    - 代码出现编译错误；
    - Widget 状态无法兼容；
    - 全局变量和静态属性的更改；
    - main 方法里的更改；
    - initState 方法里的更改；
    - 枚举和泛类型更改

### 谈谈你对Flutter渲染优化有哪些见解？
- **让 Flutter 中重建组件的个数尽量少**
在实际开发过程中，如果将整个页面写在一个单独的 StatefulWidget 中，那么每次状态更新时都会导致很多不必要的 UI 重建。因此， 我们要学会拆解组件，使用良好设计模式和状态管理方案，当需要更新状态时将影响范围降到最小。
- **构建组件时使用 const 关键词，可以抑制 widget 的重建**  
合理利用 const 关键词，可以在很大程度上优化应用的性能
使用 const 也需要注意如下几点：
    - 当 const 修饰类的构造函数时，它要求该类的所有成员都必须是final的。
    - const 变量只能在定义的时候初始化。
- **Flutter实现的一些效果背后可能会使用 saveLayer() 这个代价很大的方法**  
如下这几个组件，底层都会触发 saveLayer() 的调用，同样也都会导致性能的损耗：
    - ShaderMask
    - ColorFilter
    - Chip，当 disabledColorAlpha != 0xff 的时候，会调用 saveLayer()。
    - Text，如果有 overflowShader，可能调用 saveLayer() ，
- **官方也给了我们一些非常需要注意的优化点：**  
由于 Opacity 会使用屏幕外缓冲区直接使目标组件中不透明，因此能不用 Opacity Widget，就尽量不要用。有关将透明度直接应用于图像的示例，请参见 Transparent image，比使用 Opacity widget 更快，性能更好。  
要在图像中实现淡入淡出，请考虑使用 FadeInImage 小部件，该小部件使用 GPU 的片段着色器应用渐变不透明度。  
很多场景下，我们确实没必要直接使用 Opacity 改变透明度，如要作用于一个图片的时候可以直接使用透明的图片，或者直接使用 Container：Container(color: Color.fromRGBO(255, 0, 0, 0.5))  
Clipping 不会调用 saveLayer()（除非明确使用 Clip.antiAliasWithSaveLayer），因此这些操作没有 Opacity 那么耗时，但仍然很耗时，所以请谨慎使用。

### Flutter的内存回收管理机制
- **Dart的垃圾回收是分代的：年轻代和老年代**  
GC与Flutter engine建立联系，当engine检测到应用程序处于空闲状态且没有用户交互时，它会发出通知。这样就使得GC有了收集的窗口从而不影响性能。  

  - 年轻代 这个阶段旨在清除寿命较短的短暂对象，例如stateless widgets。虽然它是阻塞的，但它比老年代mark-sweep快得多，并且当与调度结合使用时，几乎不会影响程序的运行。  

  要确定哪些对象是否可被回收，收集器将以root对象（例如堆栈变量）开始，并检查它们引用的对象。然后把引用的对象移动到另一半空间。在那里它检查这些移动的对象指向的内容，并移动这些引用的对象。如此反复，直到移动所有活动对象到另一半空间。始终没有被引用的对象将被回收。  

  - 老年代(并行标记和并发扫描)  
  
  当对象经历过一定次数的GC仍然存在，或者其生命周期较长(个人猜测类似于element和RenderObject这种需要多次复用，可变且创建比较耗费性能)，将其放入老年代区域中。老年代采用标记整理的方法来回收对象。  
这种GC技术有两个阶段：首先遍历对象图，并标记仍在使用的对象。在第二阶段期间，扫描整个存储器，并且回收未标记的任何对象。然后清除所有标志。

### 介绍下Flutter框架，以及它的优缺点
Flutter是Google推出的一套开源跨平台UI框架，可以快速地在Android、iOS和Web平台上构建高质量的原生用户界面。同时，Flutter还是Google新研发的Fuchsia操作系统的默认开发套件。在全世界，Flutter正在被越来越多的开发者和组织使用，并且Flutter是完全免费、开源的。Flutter采用现代响应式框架构建，其中心思想是使用组件来构建应用的UI。当组件的状态发生改变时，组件会重构它的描述，Flutter会对比之前的描述，以确定底层渲染树从当前状态转换到下一个状态所需要的最小更改。
-  **优点**  
热重载（Hot Reload），利用Android Studio直接一个ctrl+s就可以保存并重载，模拟器立马就可以看见效果，相比原生冗长的编译过程强很多；  
一切皆为Widget的理念，对于Flutter来说，手机应用里的所有东西都是Widget，通过可组合的空间集合、丰富的动画库以及分层课扩展的架构实现了富有感染力的灵活界面设计；  
借助可移植的GPU加速的渲染引擎以及高性能本地代码运行时以达到跨平台设备的高质量用户体验。简单来说就是：最终结果就是利用Flutter构建的应用在运行效率上会和原生应用差不多。  
-  **缺点**  
不支持热更新；  
需要具备原生（Android/iOS）开发技能；  

### Flutter架构图
![输入图片说明](flutter_images/Imagea.png)  
由上图可知，Flutter框架自下而上分为Embedder、Engine和Framework三层。  
其中，Embedder是操作系统适配层，实现了渲染 Surface设置，线程设置，以及平台插件等平台相关特性的适配；  
Engine层负责图形绘制、文字排版和提供Dart运行时，Engine层具有独立虚拟机，正是由于它的存在，Flutter程序才能运行在不同的平台上，实现跨平台运行；  
Framework层则是使用Dart编写的一套基础视图库，包含了动画、图形绘制和手势识别等功能，是使用频率最高的一层。

### Flutter的FrameWork层和Engine层，以及它们的作用
- Flutter的FrameWork层是用Drat编写的框架（SDK），它实现了一套基础库，包含Material（Android风格UI）和Cupertino（iOS风格）的UI界面，下面是通用的Widgets（组件），之后是一些动画、绘制、渲染、手势库等。这个纯 Dart实现的 SDK被封装为了一个叫作 dart:ui的 Dart库。我们在使用 Flutter写 App的时候，直接导入这个库即可使用组件等功能。  
- Flutter的Engine层是Skia( **Impeller** ) 2D的绘图引擎库，其前身是一个向量绘图软件，Chrome和 Android均采用 Skia作为绘图引擎。Skia提供了非常友好的 API，并且在图形转换、文字渲染、位图渲染方面都提供了友好、高效的表现。Skia是跨平台的，所以可以被嵌入到 Flutter的 iOS SDK中，而不用去研究 iOS闭源的 Core Graphics / Core Animation。Android自带了 Skia，所以 Flutter Android SDK要比 iOS SDK小很多。  
    - **_Impeller 引擎_** 默认情况下，所有使用 Flutter 3.10 为 iOS 构建的应用程序都使用 Impeller。这些 iOS 应用程序将有更少的卡顿和更一致的性能。

### 介绍下Widget、State、Context 概念
- **Widget：** 在Flutter中，几乎所有东西都是Widget。将一个Widget想象为一个可视化的组件（或与应用可视化方面交互的组件），当你需要构建与布局直接或间接相关的任何内容时，你正在使用Widget。
- **Widget树：** Widget以树结构进行组织。包含其他Widget的widget被称为父Widget(或widget容器)。包含在父widget中的widget被称为子Widget。
- **Context：** 仅仅是已创建的所有Widget树结构中的某个Widget的位置引用。简而言之，将context作为widget树的一部分，其中context所对应的widget被添加到此树中。一个context只从属于一个widget，它和widget一样是链接在一起的，并且会形成一个context树。
- **State：** 定义了StatefulWidget实例的行为，它包含了用于”交互/干预“Widget信息的行为和布局。应用于State的任何更改都会强制重建Widget。

### 简述Widget的StatelessWidget和StatefulWidget两种组件类
- **StatelessWidget:** 一旦创建就不关心任何变化，在下次构建之前都不会改变。它们除了依赖于自身的配置信息（在父节点构建时提供）外不再依赖于任何其他信息。比如典型的Text、Row、Column、Container等，都是StatelessWidget。它的生命周期相当简单：初始化、通过build()渲染。
- **StatefulWidget:** 在生命周期内，该类Widget所持有的数据可能会发生变化，这样的数据被称为State，这些拥有动态内部数据的Widget被称为StatefulWidget。比如复选框、Button等。State会与Context相关联，并且此关联是永久性的，State对象将永远不会改变其Context，即使可以在树结构周围移动，也仍将与该context相关联。当state与context关联时，state被视为已挂载。StatefulWidget由两部分组成，在初始化时必须要在createState()时初始化一个与之相关的State对象。

### StatefulWidget 的生命周期 
Flutter的Widget分为StatelessWidget和StatefulWidget两种。其中，StatelessWidget是无状态的，StatefulWidget是有状态的，因此实际使用时，更多的是StatefulWidget。StatefulWidget的生命周期如下图:  
![StatefulWidget 的生命周期](flutter_images/Imageb.png)  
- initState()：Widget 初始化当前 State，在当前方法中是不能获取到 Context 的，如想获取，可以试试 Future.delayed()
- didChangeDependencies()：在 initState() 后调用，State对象依赖关系发生变化的时候也会调用。
- deactivate()：当 State 被暂时从视图树中移除时会调用这个方法，页面切换时也会调用该方法，和Android里的 onPause 差不多。
- dispose()：Widget 销毁时调用。
- didUpdateWidget：Widget 状态发生变化的时候调用。

### 简述Widgets、RenderObjects 和 Elements的关系
**Widget：** 仅用于存储渲染所需要的信息。  
**RenderObject：** 负责管理布局、绘制等操作。  
**Element：** 才是这颗巨大的控件树上的实体。  
Widget会被inflate（填充）到Element，并由Element管理底层渲染树。Widget并不会直接管理状态及渲染,而是通过State这个对象来管理状态。  
Flutter创建Element的可见树，相对于Widget来说，是可变的，通常界面开发中，我们不用直接操作Element,而是由框架层实现内部逻辑。就如一个UI视图树中，可能包含有多个TextWidget(Widget被使用多次)，但是放在内部视图树的视角，这些TextWidget都是填充到一个个独立的Element中。  
Element会持有renderObject和widget的实例。记住，Widget 只是一个配置，RenderObject 负责管理布局、绘制等操作。  
在第一次创建 Widget 的时候，会对应创建一个 Element， 然后将该元素插入树中。如果之后 Widget 发生了变化，则将其与旧的 Widget 进行比较，并且相应地更新 Element。重要的是，Element 不会被重建，只是更新而已。

### Riverpod状态管理框架
[**Flutter 状态管理：如何选择**](https://juejin.cn/post/7061784793150652452)  
[**Flutter Riverpod 全面深入解析**](https://juejin.cn/post/7063111063427874847)  
Flutter中的状态和前端React中的状态概念是一致的。React框架的核心思想是组件化，应用由组件搭建而成，组件最重要的概念就是状态，状态是一个组件的UI数据模型，是组件渲染时的数据依据。   
Flutter的状态可以分为全局状态和局部状态两种。常用的状态管理有BLoC、Redux / FishRedux、Provider、Riverpod、GetX。详细使用情况和差异可以自行了解。

### 简述Flutter的线程管理模型
默认情况下，Flutter Engine层会创建一个Isolate，并且Dart代码默认就运行在这个主Isolate上。必要时可以使用spawnUri和spawn两种方式来创建新的Isolate，在Flutter中，新创建的Isolate由Flutter进行统一的管理。 事实上，Flutter Engine自己不创建和管理线程，Flutter Engine线程的创建和管理是Embeder负责的，Embeder指的是将引擎移植到平台的中间层代码，Flutter Engine层的架构示意图如下图所示。  
![线程管理模型](flutter_images/Imagec.png)  
在Flutter的架构中，Embeder提供四个Task Runner，分别是Platform Task Runner、UI Task Runner Thread、GPU Task Runner和IO Task Runner，每个Task Runner负责不同的任务，Flutter Engine不在乎Task Runner运行在哪个线程，但是它需要线程在整个生命周期里面保持稳定。

### Flutter 是如何与原生Android、iOS进行通信的？
Flutter 通过 PlatformChannel 与原生进行交互，其中 PlatformChannel 分为三种：  
**BasicMessageChannel：** 用于传递字符串和半结构化的信息。  
**MethodChannel：** 用于传递方法调用（method invocation）。  
**EventChannel：** 用于数据流（event streams）的通信。  
同时 Platform Channel 并非是线程安全的 ，更多详细可查阅闲鱼技术的 《深入理解Flutter Platform Channel》

### 简述Flutter 的热重载
lutter 的热重载是基于 JIT 编译模式的代码增量同步。由于 JIT 属于动态编译，能够将 Dart 代码编译成生成中间代码，让 Dart VM 在运行时解释执行，因此可以通过动态更新中间代码实现增量同步。  
热重载的流程可以分为 5 步，包括：扫描工程改动、增量编译、推送更新、代码合并、Widget 重建。Flutter 在接收到代码变更后，并不会让 App 重新启动执行，而只会触发 Widget 树的重新绘制，因此可以保持改动前的状态，大大缩短了从代码修改到看到修改产生的变化之间所需要的时间。  
另一方面，由于涉及到状态的保存与恢复，涉及状态兼容与状态初始化的场景，热重载是无法支持的，如改动前后 Widget 状态无法兼容、全局变量与静态属性的更改、main 方法里的更改、initState 方法里的更改、枚举和泛型的更改等。  
可以发现，热重载提高了调试 UI 的效率，非常适合写界面样式这样需要反复查看修改效果的场景。但由于其状态保存的机制所限，热重载本身也有一些无法支持的边界。

### 什么是Flutter的三棵树？
在Flutter中和Widgets一起协同工作的还有另外两个伙伴：Elements和RenderObjects；由于它们都是有着树形结构，所以经常会称它们为三棵树。  
**Widget：** Widget是Flutter的核心部分，是用户界面的不可变描述。做Flutter开发接触最多的就是Widget，可以说Widget撑起了Flutter的半边天；  
**Element：** Element是实例化的 Widget 对象，通过 Widget 的 createElement() 方法，是在特定位置使用 Widget配置数据生成；  
**RenderObject：** 用于应用界面的布局和绘制，保存了元素的大小，布局等信息；

### 数据共享（InheritedWidget）
[**数据共享（InheritedWidget）**](https://book.flutterchina.club/chapter7/inherited_widget.html)  
InheritedWidget是 Flutter 中非常重要的一个功能型组件，它提供了一种在 widget 树中从上到下共享数据的方式，比如我们在应用的根 widget 中通过InheritedWidget共享了一个数据，那么我们便可以在任意子widget 中来获取该共享的数据！这个特性在一些需要在整个 widget 树中共享数据的场景中非常方便！如Flutter SDK中正是通过 InheritedWidget 来共享应用主题（Theme）和 Locale (当前语言环境)信息的。  
InheritedWidget和 React 中的 context 功能类似，和逐级传递数据相比，它们能实现组件跨级传递数据。InheritedWidget的在 widget 树中数据传递方向是从上到下的，这和通知Notification（将在下一章中介绍）的传递方向正好相反。

### 通知 Notification
[**通知 Notification**](https://book.flutterchina.club/chapter8/notification.html)  
通知（Notification）是Flutter中一个重要的机制，在widget树中，每一个节点都可以分发通知，通知会沿着当前节点向上传递，所有父节点都可以通过NotificationListener来监听通知。Flutter中将这种由子向父的传递通知的机制称为通知冒泡（Notification Bubbling）。通知冒泡和用户触摸事件冒泡是相似的，但有一点不同：通知冒泡可以中止，但用户触摸事件不行。  
注意：通知冒泡和Web开发中浏览器事件冒泡原理是相似的，都是事件从出发源逐层向上传递，我们可以在上层节点任意位置来监听通知/事件，也可以终止冒泡过程，终止冒泡后，通知将不会再向上传递。

### Flutter处理多个异步请求
- 使用 Future.wait()  
    Future.wait() 函数可以接收多个 Future 对象，并返回一个 Future 对象，该对象在所有 Future 对象都完成后才会完成。这样，我们可以使用该函数来等待多个异步操作完成后再进行下一步操作
    ```dart
    Future<void> fetchData() async {
      try {
        var response1 = http.get(Uri.parse('https://example.com/data1'));
        var response2 = http.get(Uri.parse('https://example.com/data2'));
        var results = await Future.wait([response1, response2]);
        var data1 = jsonDecode(results[0].body);
        var data2 = jsonDecode(results[1].body);
        print(data1);
        print(data2);
      } catch (e) {
        print('Error: $e');
      }
    }
    ```
    使用 Completer，它可以用于等待多个 Future 对象的完成，并在所有 Future 对象都完成后返回结果。使用 Completer，我们可以将多个异步请求的 Future 对象添加到 Completer 中，并通过监听 Completer 的 future 属性来等待所有 Future 对象的完成。
    ```dart
    Future<void> fetchData() async {
      try {
        var completer = Completer<List<dynamic>>();
        var response1 = http.get(Uri.parse('https://example.com/data1'));
        var response2 = http.get(Uri.parse('https://example.com/data2'));
        Future.wait([response1, response2]).then((results) {
          var data1 = jsonDecode(results[0].body);
          var data2 = jsonDecode(results[1].body);
          completer.complete([data1, data2]);
        });
        var results = await completer.future;
        print(results[0]);
        print(results[1]);
      } catch (e) {
        print('Error: $e');
      }
    }
    ```
- 使用 Stream 和 StreamController  
    Stream 和 StreamController 是 Dart 中用于处理流的类，它们可以用于实现多个异步请求的并发处理和结果组合。我们可以创建多个 StreamController，并将它们的流合并到一个单独的流中，然后使用 Stream 的各种操作符来处理结果。
    ```dart
    Future<void> fetchData() async {
      try {
        var controller1 = StreamController();
        var controller2 = StreamController();
        http.get(Uri.parse('https://example.com/data1'))
            .then((response) => controller1.add(response.body));
        http.get(Uri.parse('https://example.com/data2'))
            .then((response) => controller2.add(response.body));
        var results = await StreamZip([controller1.stream, controller2.stream]).toList();
        var data1 = jsonDecode(results[0]);
        var data2 = jsonDecode(results[1]);
        print(data1);
        print(data2);
      } catch (e) {
        print('Error: $e');
      }
    }
    ```
- 使用 async* 和 yield  
    async* 和 yield 是 Dart 中用于创建异步生成器的语法，它们可以用于实现多个异步请求的串行处理和结果组合。我们可以使用 yield 关键字将每个异步请求的结果返回给调用方，从而实现异步请求的串行处理。
    ```dart
    Stream<String> fetchData() async* {
      try {
        var response1 = await http.get(Uri.parse('https://example.com/data1'));
        var data1 = jsonDecode(response1.body);
        yield data1.toString();
        var response2 = await http.get(Uri.parse('https://example.com/data2'));
        var data2 = jsonDecode(response2.body);
        yield data2.toString();
      } catch (e) {
        print('Error: $e');
      }
    }
    ```
- 使用 Isolate  
    Isolate 是 Dart 中的一种轻量级进程，它可以与其他 Isolate 并行执行，并且与其他 Isolate 共享内存空间。通过使用 Isolate，我们可以在多个独立的 Isolate 中同时执行多个异步请求，并将结果传回主 Isolate 进行处理。
    ```dart
    import 'dart:isolate';
    
    void fetchData(SendPort sendPort) async {
      try {
        var response1 = await http.get(Uri.parse('https://example.com/data1'));
        var data1 = jsonDecode(response1.body);
        var response2 = await http.get(Uri.parse('https://example.com/data2'));
        var data2 = jsonDecode(response2.body);
        sendPort.send([data1, data2]);
      } catch (e) {
        sendPort.sendError('Error: $e');
      }
    }
    
    Future<void> main() async {
      try {
        var receivePort = ReceivePort();
        await Isolate.spawn(fetchData, receivePort.sendPort);
        var results = await receivePort.first;
        print(results[0]);
        print(results[1]);
      } catch (e) {
        print('Error: $e');
      }
    }
    ```

### Flutter和Android原生通信的三种方式
- Flutter 与原生之间的通信依赖灵活的消息传递方式
    - 应用的Flutter部分通过平台通道（platform channel）将消息发送到其应用程序的所在的宿主（iOS或Android）应用（原生应用）
    - 宿主监听平台通道，并接收该消息。然后它会调用该平台的 API，并将响应发送回客户端，即应用程序的 Flutter 部分
- Flutter 与原生存在三种交互方式
    - BasicMessageChannel：用于传递字符串和半结构化的信息，这个用的比较少
    - EventChannel：用于数据流（event streams）的通信。有监听功能，比如电量变化之后直接推送数据给flutter端
    - MethodChannel：用于传递方法调用（method invocation）通常用来调用 native 中某个方法
- 三种 Channel 之间互相独立，各有用途，但它们在设计上却非常相近。每种 Channel 均有三个重要成员变量
    - name: String类型，代表 Channel 的名字，也是其唯一标识符
    - messager：BinaryMessenger 类型，代表消息信使，是消息的发送与接收的工具
    - codec: MessageCodec 类型或 MethodCodec 类型，代表消息的编解码器
1. BasicMessageChannel  
    双向通信，,有返回值，主要用于传递字符串和半结构化的信息。  
    - Flutter端
        1. 创建BasicMessageChannel对象并定义channel的name，必须和Android一端保持一致。
        ```dart
         late BasicMessageChannel<String> _messageChannel;
         _messageChannel = const BasicMessageChannel<String>("baseMessageChannel", StringCodec());
        ```
        2. 设置setMessageHandler监听Android原生发送的消息。
        ```dart
        _messageChannel.setMessageHandler((message) async {
          print("flutter :Message form Android reply:$message");
          return "flutter already received reply ";
        });
        ```
        3. 发送消息给Android原生。
        ```dart
          _messageChannel.send("Hello Android,I come form Flutter");
        ```
    - Android端
        1. 初始化BasicMessageChannel,定义Channel名称。
        ```kotlin
        val messageChannel = BasicMessageChannel<String>(
            flutterEngine.dartExecutor.binaryMessenger,
            "baseMessageChannel",
            StringCodec.INSTANCE
        )
        ```
        2. 设置setMessageHandler监听来自Flutter的消息。
        ```kotlin
        messageChannel.setMessageHandler { message, reply ->
            Log.i("flutter", "android receive message form flutter :$message")
        }
        ```
        3. 主动发送消息给Flutter。
        ```kotlin
        messageChannel.send("flutter")
        ```
    - 打印结果如下：  
    ![输入图片说明](flutter_images/flutter1218-1.png.png)  
    从用法上来看，Flutter和Android原生基本上是一样的，只不过是不同语言的不同写法。Flutter端主动调用send方法将消息传递给Android原生。然后打印log日志。

2. EventChannel  
    单向通信，是一种Android native向Flutter发送数据的单向通信方式,Flutter无法返回任何数据给Android native。主要用于Android native向Flutter发送手机电量变化、网络连接变化、陀螺仪、传感器等信息，主要用于数据流的通信
    - Flutter端
        1. 创建EventChannel对象，并给定channel名称。
        ```dart
        late EventChannel _eventChannel;
        _eventChannel = const EventChannel("eventChannel");
        ```
        2. 使用receiveBroadcastStream接收Android端的消息。
        ```dart
        _eventChannel.receiveBroadcastStream().listen( (event){
            print("Flutter:Flutter receive from Android:$event");
        },onDone: () {
            print("完成");
        },onError: (error) {
            print("失败：$error");
        });
        ```
    - Android端
        1. 创建EventChannel对象，并定义channel的name。
        ```kotlin
         val eventChannel  = EventChannel(flutterEngine.dartExecutor.binaryMessenger,"eventChannel")
        ```
        2. 设置setStreamHandler监听。
        ```kotlin
        eventChannel.setStreamHandler(object :StreamHandler{
            override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
                eventSink = events
            }
            override fun onCancel(arguments: Any?) {
            }
        })
        ```
        3. 发送消息给Flutter端。
        ```kotlin
        Handler(mainLooper).postDelayed({
            Log.i("flutter", "android send message")
            eventSink?.success("Hello Flutter")
        },500)
        ```
    - 打印结果如下：  
    ![输入图片说明](flutter_images/image1218-2.png.png)
    EventChannel是单向通信，并且只能从Android原生端发送消息给Flutter，Flutter不可以主动发消息，使用有局限性。

3. MethodChannel  
    双向异步通信，Flutter和原生端都可以主动发起，同时可以互相传递数据，用于传递方法调用。
    - Flutter端
        1. 创建MethodChannel对象，并定义通道名称。
        ```dart
        late MethodChannel _methodChannel;
        _methodChannel = const MethodChannel('methodChannel');
        ```
        2. 设置setMethodCallHandler异步监听消息。
        ```dart
         _methodChannel.setMethodCallHandler((call) async {

    
        });
        ```
        3. 调用invokeMethod方法发送消息给Android，并接收返回值。
        ```dart
        var map = {'name':'小明','age':18};
        String result  = await _methodChannel.invokeMethod('openMethodChannel',map);
        print("Flutter接收Android返回的结果:$result");
        ```
    - Android端
        1. 创建MethodChannel对象，并定义通道名称。
        ```kotlin
        val methodChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger,"methodChannel")
        ```
        2. 设置setMethodCallHandler监听Flutter消息，并通过MethodCall.method区分方法名。
        ```kotlin
        methodChannel.setMethodCallHandler { call, result ->
            if (call.method == "openMethodChannel"){
                val  name = call.argument<String>("name")
                val age = call.argument<Int>("age")
                Log.i("flutter", "android receive form:$name ,$age ")
                result.success("success")
            }
        }
        ```
        3. 发送消息给Flutter。
        ```kotlin
        messageChannel.send("Hello Flutter")
        ```
    - 打印结果如下：  
    ![输入图片说明](flutter_images/image1218-3.png.png)  
    MethodChannel是Flutter和Android原生常用的通信方式，不仅可以异步通信，还能传递数据集合，Map等。通过定义不同的方法名，调用Android不同的功能。

- 总结  
本文主要介绍Flutter与Android原生的三种通信方式，可以根据实际开发中的业务，选择合适的通信方式。熟练掌握三种通信方式可以在Flutter开发中使用Android原生提供的功能。
        
### Flutter Key 的原理和使用  
[**Flutter Key 的原理和使用**](https://juejin.cn/post/6976069994270588941)  
[**Flutter 深入浅出Key**](https://juejin.cn/post/6844903811870359559)  
加上 key 之后，widget 和 element 会有对应关系，如果 key 没有对应就会重新在同层级下寻找，如果没有最终这个 widget 或者 Element 就会被删除  
Key在具有相同父级的 [Element] 中必须是唯一的。相比之下，[GlobalKey] 在整个应用程序中必须是唯一的。  
- LocalKey 的三种类型  
    LocalKey 继承自 Key， 翻译过来就是局部键，LocalKey 在具有相同父级的 Element 中必须是惟一的。也就是说，LocalKey 在同一层级中必须要有唯一性。  
    LocalKey 有三种子类型，下面我们来看一下:
    - ValueKey  
        ```dart
        class ValueKey<T> extends LocalKey {
          final T value;
          const ValueKey(this.value);
        
        
          @override
          bool operator ==(Object other) {
            if (other.runtimeType != runtimeType)
              return false;
            return other is ValueKey<T>
                && other.value == value;
          }
        }
        ```
        使用特定类型的值来标识自身的键，ValueKey 在最上面的例子中已经使用过了，他可以接收任何类型的一个对象来最为 key。  
        通过源码我们可以看到它重写了 == 运算符，在判断是否相等的时候首先判断了类型是否相等，然后再去判断 value 是否相等；
    - ObjectKey
        ```dart
        class ObjectKey extends LocalKey {
          const ObjectKey(this.value);
          final Object? value;
        
          @override
          bool operator ==(Object other) {
            if (other.runtimeType != runtimeType)
              return false;
            return other is ObjectKey
                && identical(other.value, value);
          }
        
          @override
          int get hashCode => hashValues(runtimeType, identityHashCode(value));
        }        
        ```
        ObjectKey 和 ValueKey 最大的区别就是比较的算不一样，其中首先也是比较的类型，然后就调用 indentical 方法进行比较，其比较的就是内存地址，相当于 java 中直接使用 == 进行比较。而 LocalKey 则相当于 java 中的 equals 方法用来比较值的。
        > 需要注意的是使用 ValueKey 中使用 == 比较的时候，如果没有重写 hashCode 和 == ，那样即使 对象的值是相等的，但比较出来也是不相等的。所以说尽量重写吧！
    - UniqueKey
        ```dart
        class UniqueKey extends LocalKey {
          UniqueKey();
        }    
        ```
        很明显，从名字中可以看出来，这是一个独一无二的 key。
每次重新 build 的时候，UniqueKey 都是独一无二的，所以就会导致无法找到对应的 Element，状态就会丢失。那么在什么时候需要用到这个 UniqueKey呢？我们可以自行思考一下。  
        还有一种做法就是把 UniqueKey 定义在 build 的外面，这样就不会出现状态丢失的问题了。
- GlobalKey  
    GlobalKey 继承自 Key，相比与 LocalKey，他的作用域是全局的，而 LocalKey 只作用于当前层级。  
    在之前我们遇到一个问题，就是如果给一个 Widget 外面嵌套了一层，那么这个 Widget 的状态就会丢失，如下：  
    ```dart
     children: <Widget>[
        Box(Colors.red),
        Box(Colors.blue),
      ],
      
     ///修改为如下，然后重新 build
      children: <Widget>[
        Box(Colors.red),
        Container(child:Box(Colors.blue)),
      ],
    ```  
    原因在之前我们也讲过，就是因为类型不同。只有在类型和 key 相同的时候才会保留状态 ，显然上面的类型是不相同的；  
    那么遇到这种问题要怎么办呢，这个时候就可以使用 GlobalKey 了。我们看下面的栗子：  
    ```dart
    class Counter extends StatefulWidget {
      Counter({Key key}) : super(key: key);
    
      @override
      _CounterState createState() => _CounterState();
    }
    
    class _CounterState extends State<Counter> {
      int _count = 0;
    
      @override
      Widget build(BuildContext context) {
        return RaisedButton(
          onPressed: () => setState(() => _count++),
          child: Text("$_count", style: TextStyle(fontSize: 70)),
        );
      }
    }
    ```
    ```dart
    final _globalKey = GlobalKey();
    
    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Counter(),
              Counter(),
            ],
          ),
        ),
      );
    }
    ```
    上面代码中，我们定义了一个 Counter 组件，点击后 count 自增，和一个 GlobakKey 的对象。
    
### Flutter基础之路由详解
Flutter中管理多个页面时有两个核心概念和类：Route和Navigator。  
一个route是一个屏幕或页面的抽象，Navigator是管理route的Widget。Navigator可以通过route入栈和出栈来实现页面之间的跳转。
路由一般分为**静态路由(即命名路由)**和**动态路由**。
- 路由导航
    - 静态路由(即命名路由)  
        静态路由在通过Navigator跳转之前，需要在MaterialApp组件内显式声明路由的名称，而一旦声明，路由的跳转方式就固定了。通过在MaterialApp内的routes属性进行显式声明路由的定义。
        ```dart
        class MyApp extends StatelessWidget {
          @override
          Widget build(BuildContext context) {
            return MaterialApp(
              initialRoute: "/", // 默认加载的界面，这里为RootPage
              routes: { // 显式声明路由
               // "/":(context) => RootPage(),
                "/A":(context) => Apage(),
                "/B":(context) => Bpage(),
                "/C":(context) => Cpage(),
              },
              home: RootPage(),
            );
          }
        }
        /// 注意：如果指定了home属性，routes表则不能再包含此属性。
        /// 如上代码中【home: RootPage()】  和 【"/":(context) => RootPage()】两则不能同时存在。
        ```
        ```dart
        /// 例如：RootPage跳转Apage即：RootPage —>Apage
        Navigator.of(context).pushNamed("/A");
        ```
        一般方法中带有Name多数是通过静态路由完成跳转的，如pushNamed、pushReplacementNamed、pushNamedAndRemoveUntil等。
    - 动态路由  
        动态路由无需在MaterialApp内的routes中注册即可直接使用：RootPage  —> Apage
        ```dart
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => Apage(),
        ));
        ```
        动态路由中，需要传入一个Route,这里使用的是MaterialPageRoute ，它可以使用和平台风格一致的路由切换动画,在iOS上左右滑动切换，Android上会上下滑动切换。也可以使用CupertinoPageRoute 实现全平台的左右滑动切换。
        当然也可以自定义路由切换动画，使用PageRouteBuilder:使用FadeTransition 做一个渐入过渡动画。
        ```dart
        Navigator.of(context).push(
          PageRouteBuilder(
            transitionDuration: Duration(milliseconds: 250), // //动画时间为0.25秒
            pageBuilder: (BuildContext context,Animation animation,
                Animation secondaryAnimation){
              return FadeTransition( //渐隐渐入过渡动画
                opacity: animation,
                child: Apage()
               );
             }
          )
        );
        ```
        到现在为止，可能对路由有了一定的认识，下面就结合具体方法来详细说明。  
        在这之前有必要说明：
    
                Navigator.of(context).push和Navigator.push两着并没有特别的区别，看源码也得知，后者其实就是调用了前者。  
        of(context)：获取Navigator当前已存在实例的状态。  
    - pop
        返回当前路由栈的上一个界面。
        ```dart
        Navigator.pop(context);
        ```
    - 不同路由方法的使用
        - push / pushNamed  
        两者运行效果相同，只是调用不同，都是将一个page压入路由栈中。直白点就是push是把界面直接放入，pushNames是通过路由名的方式，通过router使界面进入对应的栈中。 结果：直接在原来的路由栈上添加一个新的 page。
        - pushReplacement / pushReplacementNamed / popAndPushNamed  
        替换路由，顾名思义替换当前的路由。 例如：  
        ![输入图片说明](flutter_images/route-1.png.png)  
        由图可知在BPage使用替换跳转到Cpage的时候，Bpage被Cpage替换了在堆栈中的位置而移除栈，CPage默认返回的是APage。
        - pushAndRemoveUntil / pushNamedAndRemoveUntil  
        在使用上述方式跳转时，会按次序移除其他的路由，直到遇到被标记的路由（predicate函数返回了true）时停止。若 没有标记的路由，则移除全部。 当路由栈中存在重复的标记路由时，默认移除到最近的一个停止。  
            1. 第一种方式
                ```dart
                /// 移除全部
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => CPage()), 
                    (Route router) => router == null
                );
                ```
                或者
                ```dart
                /// 移除全部
                Navigator.of(context).pushNamedAndRemoveUntil("/C", (Route router) => router == null);
                ```
                此时的路由栈示意图：  
                ![输入图片说明](flutter_images/route-2.png.png)  
                可知除了要push的CPage，当前路由栈中所有的路由都被移除，CPage变成根路由。  
            2. 第二种：移除到RootPage停止
                ```dart
                ///  "/"即为RootPage，标记后，移除到该路由停止移除
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => CPage()), 
                    ModalRoute.withName('/')
                );
                或
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (_) => CPage()), 
                    (Route router) => router.settings.name == "/"
                );
                /// 以上两种只是写法不一样
                ```
                或
                ```dart
                Navigator.of(context).pushNamedAndRemoveUntil("/C", (Route router) => router.settings.name == "/");
                或
                Navigator.of(context).pushNamedAndRemoveUntil("/C", ModalRoute.withName("/"));
                ```
                此时的路由栈示意图：  
                ![输入图片说明](flutter_images/route-3.png.png)  
                push到CPage的时候，移除到RootPage停止,CPage默认返回RootPage。
        - popUntil  
            返回到指定的标记路由，若标记的路由为null，则程序退出，慎用！！！
有时候我们需要根据业务需求判断：可能返回上一级路由，也可能返回上上级路由或是返回指定的路由等。这个时候就不能使用Replacement和RemoveUntil来替换、移除路由了。例如：  
            ![输入图片说明](flutter_images/popuntil.png)  
            ```dart
            Navigator.of(context).popUntil((route) => route.settings.name == "/");
            或
            Navigator.of(context).popUntil(ModalRoute.withName("/"));
            ```
            再例如：  
            ![输入图片说明](flutter_images/popuntil-2.png)
            要实现上述功能，从CPage返回到APage，并且不在MaterialApp内的routes属性进行显式声明路由。因为笔者觉得一个应用程序的界面太多了，如果每个界面都要显示声明路由，实在是不优雅。
因为需要返回APage，还是需要标记路由，所有我们在之前跳转APage的时候设置RouteSettings，如下：  
            ```dart
            /// 设置APage的RouteSettings
            Navigator.of(context).push(MaterialPageRoute(
              settings: RouteSettings(name:"/A"),
              builder: (context) => APage(),
            ));
            ```
            在CPage需要返回的时候，调用就行：
            ```dart
            Navigator.of(context).popUntil(ModalRoute.withName("/A"));
            ```
            这样代码看起来很优雅，不会冗余。 另：
            ```dart
            /// 返回根路由
            Navigator.of(context).popUntil((route) => route.isFirst);
            ```
        - canPop  
        用来判断是否可以导航到新页面，返回的bool类型，一般是在设备带返回的物理按键时需要判断是否可以pop。
        - maybePop  
        可以理解为canPop的升级，maybePop 会自动判断。如果当前的路由可以pop，则执行当前路由的pop操作，否则将不执行。
        - removeRoute/removeRouteBelow  
            - removeRoute  
            删除路由，同时执行Route.dispose操作，无过渡动画，正在进行的手势也会被取消。
            ![removeRoute](flutter_images/removeroute.png)  
            BPage被移除了当前的路由栈。 如果在当前页面调用removeRoute ，则类似于调用pop方法，区别就是无过渡动画，所以removeRoute 也可以用来返回上一页。
            - removeRouteBelow  
            移除指定路由底层的临近的一个路由，并且对应路由不存在的时候会报错。 同上。

            综上：这个两个方法一般情况下很少用，而且必须要持有对应的要移除的路由。 一般用于立即关闭，如移除当前界面的弹出框等。

- 路由传值  
    见：[**Flutter基础之路由详解**](http://juejin.cn/post/7055590620072509453)


