# Android面试题

### 面向对象
**封装** 也就是把客观事物封装成抽象的类，把自己的数据和方法只让可信的类或者对象操作，对不可信的类进行信息隐藏。  
**继承** 是指这样一种能力：它可以使用现有类的所有功能，并在无不需要原来的类的情况下对这些功能进行扩展。  
**多态** 性是允许你将父对象设置成为和一个或更多的他的子对象相等的技术，赋值之后，父对象就可以根据当前赋值给它的子对象的特性以不同的方式运作;可以说就是允许将子类类型的指针赋值给父类类型的指针。  
**覆盖** 是指子类重新定义父类的虚函数的做法。  
**实现多态，有二种方式，重写，重载**  
- 重载,方法名相同,参数类型相同,子类的返回值类型大于等于父类,修饰符子类的访问权限要大于等于父类,子类抛出的异常不能大于父类.
- 重载，是指允许存在多个同名函数，而这些函数的参数表不同（或许参数个数不同，或许参数类型不同，或许两者都不同）。同一方法中方法名相同,参数列表不同,参数的个数,类型,顺序(具体使用自定义adapter)  
- Java中实现多态的机制是什么？  
答：方法的重写Overriding和重载Overloading是Java多态性的不同表现  
重写Overriding是父类与子类之间多态性的一种表现  
重载Overloading是一个类中多态性的一种表现  
- Java中重载和重写的区别：
    1. 重载：一个类中可以有多个相同方法名的，但是参数类型和个数都不一样。这是重载。
    2. 重写：子类继承父类，则子类可以通过实现父类中的方法，从而新的方法把父类旧的方法覆盖。

### MVC MVP MVVM 框架
**相同的目的：** 目标都是解耦，解耦好处一个是关注点分离，提升代码可维护和可读性，并且提升代码复用性，都将应用抽象分离成视图、逻辑、数据3层。  
- **MVC**  
MVC将应用抽象为数据层（Model）、视图（View）、逻辑（Controller），各层之间的通信模式则没有严格限制。  
虽然说mvc的3个模块明确，但是Activity的代码越来越多。model和Controller也越来越耦合，因为Activity里的View的代码跟Model的代码会堆积在一起。View层的逻辑与Model层的逻辑都在Activity里，因此View与Model层根本就没有解耦，也就是说并没有达到三者互相分离的效果，反而会随着代码的增多，逻辑也会越来越复杂。  
所以MVC的缺点可以总结为：逻辑全在Controller里面实现的，View模块与Model木块并没有完全分离。  
    - MVC的优点：   
        1. 耦合性低,视图层和业务层分离，这样更改视图层就不会从新编译控制器代码  
        2. 重用性高  
        3. 生命周期成本低  
        4. 可维护性高，分离视图层和业务逻辑层使应用更容易维护和修改  
    - MVC的缺点：  
        1. 不适合小型和中型的应用程序  
        2. 视图与控制器过于紧密连接  
        3. 视图对模型数据的访问效率低  
        4. Model和View可能有耦合，相当于MVC只是将应用抽象，并未限制数据流。  
- **MVP**  
他就是限定通信方式 ,让Model不能和View直接通信,而是通过Presenter通信 这样让Model和View解耦更彻底 代码更容易复用
他的Presenter相当于MVC中的Controller代表逻辑层
简单来说就是他弥补了mvc的View与Model没有分离，耦合严重这些缺点;
在MVP框架中，Model负责数据，Presenter负责逻辑处理，View就负责UI;
在mvp分层的过程中，他给Model编写了接口，这样更加解耦，因为它增加接口把对对象的依赖变成了对接口的依赖，可以说是面向接口编程的编程思想;
在这个框架中View和Model是不联立的，这时候Presenter起到一个桥梁的作用。他就是在Presenter中创建了View和Model的对象，首先获取View模块中UI的数据，然后作为参数传到Model的方法中，最后在回调给View模块，这样就实现了View与Model的交互了，最后在Activity中创建Presenter对象并且调用它的方法。
总结一下就是MVP是真的实现了View，Model与Presenter的分离，耦合度相比MVC来说要少很多，当然，虽然逻辑还是堆在了Activity里，但Activity不用担心数据和UI，因为Activity只负责调用Presenter、view的方法就可以，具体细节的改动就去View和Model中修改改。
    - MVP的优点：
        1. 模型和视图完全隔离，我们可以更改视图而不影响模型
        2. 可以高效率的使用模型，因为所有的交互都发生在presenter层
        3. 我们可以将presenter用于多个视图，而不需要改变presenter的逻辑
    - MVP的缺点：
        1. View和presenter层的交互太频繁，一旦View变了，presenter层也要变
        2. Presenter负担过重 并且Presenter需要知道Model和View的结构 并且数据变化的时候需要手动更新到View上 这增加了编码负担 代码维护性差(下降)
    - MVP内存泄露问题  
        Presenter中持有 View接口对象，这个接口对象实际为MainActivity.this, Modle中也同时拥有Presenter对象实例，当MainActivity要销毁时，
Presenter中有Modle在获取数据，那么问题来了，这个Activity就不能正常销毁了;  
        当Modle在获取数据时，不做处理，它就一直持有Presenter对象，而Presenter对 象又持有Activity对象，这条GC链不剪断，Activity就无法 被完整回收。
        换句话说: Presenter不销毁， Activity就无法 正常被回收。
    - 解决MVP的内存泄露  
        Presenter在Activity的onDestroy方法回调时执行资源释放操作，或者在Presenter弓|用View对象时使用更加容易回收的软引用，弱应用。  
- **MVVM**  
MVVM设计了VM(ViewModel),ViewModel采用DataBinding双向绑定让数据自动同步数据到View上用MVVM中的VM代替MVP中的Presenter后 MVVM自动从Model以模板渲染的方式映射到View上 不需要用户手动操作视图
    - MVVM的优点
        1. 双向绑定技术，当Model变化时，View-Model会自动更新，View也会自动变化。很好做到数据的一致性. MVVM模式有些时候又被称作：model-view-binder模式。
        2. View的功能进一步的强化，具有控制的部分功能，若想无限增强它的功能，甚至控制器的全部功能几乎都可以迁移到各个View上（不过这样不可取，那样View干了不属于它职责范围的事情）。View可以像控制器一样具有自己的View-Model.
        3. 由于控制器的功能大都移动到View上处理，大大的对控制器进行了瘦身。不用再为看到庞大的控制器逻辑而发愁了。
    - MVVM的缺点
        1. 数据绑定使得 Bug 很难被调试。你看到界面异常了，有可能是你 View 的代码有 Bug，也可能是 Model 的代码有问题。数据绑定使得一个位置的 Bug 被快速传递到别的位置，要定位原始出问题的地方就变得不那么容易了。
        2. 一个大的模块中，model也会很大，虽然使用方便了也很容易保证了数据的一致性，当时长期持有，不释放内存，就造成了花费更多的内存。
        3. 数据双向绑定不利于代码重用。客户端开发最常用的重用是View，但是数据双向绑定技术，让你在一个View都绑定了一个model，不同模块的model都不同。那就不能简单重用View了。  
- **MVC MVP MVVM三种框架的总结**  
这3种应用框架是递进关系 不断优化的  
    1. MVC:Model和View还有一定的耦合
    2. MVP Model和View彻底分开 只让Presenter调用
    3. MVVM通过自动同步数据更新到视图,解决MVP中手动同步的缺点 这样更简化代码
    - MVC MVP MVVM之间的区别：
        - MVC将应用抽象为数据层（Model）、视图层（View）、逻辑层（controller），降低了项目耦合。但MVC并未限制数据流，Model和View之间可以通信。
        - MVP则限制了Model和View的交互都要通过Presenter，这样对Model和View解耦，提升项目维护性和模块复用性。
        - MVVM是对MVP的P的改造，用VM替换P，将很多手动的数据=>视图的同步操作自动化，降低了代码复杂度，提升可维护性。

### 什么是MVVM？
它抽离了视图、数据和逻辑，并限定了Model和View只能通过VM进行通信，VM订阅Model并在数据更新时候自动同步到视图。  
DataBinding如何实现双向绑定的：  
![输入图片说明](android_images/imagea.png)  
DataBinding在编译时会生成一个ViewDataBinding的子类xxBinding，例如ActivityMainBinding，ActivityMainBinding默认持有了View和Model和Activity/Fragment，在ActivityMainBinding里面有个excuteBinding()方法,在这里为View设置值的，同时设置了BindingAdapter和InverseBindingLinstener。BindingAdapter主要是提供了View的set get方法和状态Linstener。  
- viewModel的生命周期  
ViewModel对象存在的时间范围是获取ViewModel时传递给ViewModelProvider的Lifecycle。ViewModel将一直留在内存中，直到限定其存在时间范围的Lifecycle永久消失：对于Activity，是在Activity完成时；而对于Fragment，是在Fragment分离时。  
当Activity处于前台的时候被销毁了，那么得到的ViewModel是之前实例过的ViewModel；如果Activity处于后台时被销毁了，那么得到的ViewModel不是同一个。举例说，如果 Activity 因为配置发生变化而被重建了，那么当重建的时候，ViewModel是之前的实例；如果因为长期处于后台而被销毁了，那么重建的时候，ViewModel就不是之前的实例了。

###  OKHttp的优点
- 使用简单，在设计时使用了外观模式，将整个系统的复杂性给隐藏起来，将子系统接口通过一个客户端OkHttpClient统一暴露出来。  
- 扩展性强，可以通过自定义应用拦截器与网络拦截器，完成用户各种自定义的需求  
- 功能强大，支持Spdy、Http1.X、Http2、以及WebSocket等多种协议  
- 通过连接池复用底层TCP(Socket)，减少请求延时  
- 无缝的支持GZIP减少数据流量  
- 支持数据缓存，减少重复的网络请求  
- 支持请求失败自动重试主机的其他ip，自动重定向  
- 网络优化方面：
    1. 内置连接池，支持连接复用；
    2. 支持gzip压缩响应体；
    3. 通过缓存避免重复的请求；
    4. 支持http2，对一台机器的所有请求共享同一个socket。
- 功能方面  
    满足了网络请求的大部分请求。
- 扩展性方面  
    责任链模式使得很容易添加一个自定义拦截器对返回结果进行处理。
- OKHttp和Retrofit的关系区别：
    - Retrofit封装了具体的请求，线程切换以及数据转换。
    - retrofit通过使用代理，外观，策略模式对okhttp进行了封装
    - OkHttp 是基于Http协议封装的一套请求客户端
 
### HttpClient与HttpUrlConnection的区别
- HttpClient和HttpUrlConnection 这两种方式都支持Https协议，都是以流的形式进行上传或者下载数据，也可以说是以流的形式进行数据的传输，还有ipv6,以及连接池等功能。  
- HttpClient这个拥有非常多的API，所以如果想要进行扩展的话，并且不破坏它的兼容性的话，很难进行扩展，也就是这个原因，Google在Android6.0的时候，直接就弃用了这个HttpClient.  
- HttpUrlConnection相对来说就是比较轻量级了，API比较少，容易扩展，并且能够满足Android大部分的数据传输。比较经典的一个框架volley，在2.3版本以前都是使用HttpClient,在2.3以后就使用了HttpUrlConnection。  

### Retrofit
Retrofit就是一个网络请求框架的封装，底层的网络请求默认使用的Okhttp，本身只是简化了用户网络请求的参数配置等，还能与Rxjava相结合，使用起来更加简洁方便。  
**Retrofit 涉及到的设计模式：** 外观模式，构建者模式，工厂模式，代理模式，适配器模式，策略模式，观察者模式  
- App应用程序通过Retrofit请求网络，实际上是使用Retrofit接口层封装请求参数，之后由OkHttp完成后续的请求操作。
- 在服务端返回数据之后，OkHttp将原始的结果交给Retrofit，Retrofit根据用户的需求对结果进行解析。
- 完成数据的转化(converterFactory)，适配(callAdapterFactory)，通过设计模式进行各种扩展。
- Retrofit基本的使用
    1. 通过Retrofit的builder模式创建一个retrofit对象，可以在builder里面设置okhttpclient、baseurl、callAdapterFactroy、addConverterFactory等信息。
    2. 通过得到的retrofit对象加工要请求的接口，得到一个接口对象。
    3. 通过这个接口对象，调用接口里面的方法得到一个网络工作对象。
    4. 通过这个网络工作对象调用enqueue方法或者execute方法发起请求并且回调回请求的结果。
- Retrofit的定制扩展主要分为三个方面
    1. 用谁来生成网络工作对象，在retrofit中虽然只能传入一个OkhttpClient对象，但是okhttp有丰富的扩展性，可以定制自己的拦截器，可以实现数据的缓存等。
    2. 生成的网络工作对象进行转换（我们实际进行请求的是okhttp3里面的realCall，而在网络接口中定义的Call对象是在retrofit2中定义的，所以需要一个适配器进行转换），retrofit里面添加rxJava支持，可以返回一个Observable对象。
    3. 对网络返回的数据进行解析，可以添加自己的数据解析器，定义自己的convertor类和ConvertorFactory类。

### RxJava 的观察者模式
RxJava 有四个基本概念：**Observable** (被观察者)、 **Observer** (观察者)、 **Subscribe** (订阅)、**事件**。Observable 和 Observer 通过 subscribe() 方法实现订阅关系，从而 Observable 可以在需要的时候发出事件来通知Observer。  
与传统观察者模式不同， RxJava 的事件回调方法除了普通事件 onNext()之外，还定义了两个特殊的事件：onCompleted()和onError()。  
onCompleted(): 事件队列完结。RxJava 不仅把每个事件单独处理，还会把它们看做一个队列。RxJava 规定，当不会再有新的onNext()发出时，需要触发 onCompleted() 方法作为标志。  
onError(): 事件队列异常。在事件处理过程中出异常时，onError() 会被触发，同时队列自动终止，不允许再有事件发出。onCompleted() 和 onError() 二者也是互斥的，即在队列中调用了其中一个，就不应该再调用另一个。

### Okhttp原理
OkHttp请求过程中最少只需要接触OkHttpClient、Request、Call、 Response，但是框架内部会进行大量的逻辑处理。  
所有网络请求的逻辑大部分集中在拦截器中，但是在进入拦截器之前还需要依靠分发器来调配请求任务。  
**分发器：** 内部维护队列与线程池，完成请求调配。  
**拦截器：** 五大默认拦截器完成整个请求过程。  
- **网络请求过程:**  
通过建造者模式构建OKHttpClient与Request  
OKHttpClient通过newCall发起一个新的请求  
Okhttp会通过Dispatcher对我们所有的RealCall进行统一管理，并通过execute及enqueue方法对同步或异步请求进行处理。  
通过分发器维护请求队列与线程池，完成请求调配。  
通过五大默认拦截器完成请求重试，缓存处理，建立连接这些操作,得到网络请求结果。  
- **OKHttp分发器是怎样工作的？**  
分发器的主要作用是维护请求队列与线程池,比如我们有100个异步请求，肯定不能把它们同时请求，而是应该把它们排队分个类，分为正在请求中的列表和正在等待的列表，等请求完成后，即可从等待中的列表中取出等待的请求，从而完成所有的请求。
- **同步请求各异步请求也不相同**  
同步请求:因为同步请求不需要线程池，也不存在任何限制。所以分发器仅做一下记录。后续按照加入队列的顺序同步请求即可。  
异步请求:当正在执行的任务未超过最大限制64，同时同一Host的请求不超过5个，则会添加到正在执行队列，同时提交给线程池。否则先加入等待队列。每个任务完成后，都会调用分发器的finished方法，这里面会取出等待队列中的任务继续执行。  
经过分发器的任务分发，然后就要利用拦截器开始一系列配置了。  
- **OKHttp拦截器是怎样工作的？**  
RealCall的execute方法，可以看出，最后返回了getResponseWithInterceptorChain，责任链的构建与处理其实就是在这个方法里面。  
责任链，顾名思义，就是用来处理相关事务责任的一条执行链，执行链上有多个节点，每个节点都有机会（条件匹配）处理请求事务，如果某个节点处理完了就可以根据实际业务需求传递给下一个节点继续处理或者返回处理完毕。  

### OKHttp拦截器
**应用拦截器：** 拿到的是原始请求，可以添加一些自定义header.通用参数、参数加密、网关接入等等。  
- **RetryAndFollowUpInterceptor(重定向拦截器)：** 负责重试或请求重定向  
    原理：构建一个Streamllocation对象，然后调用下一个拦截器获取结果，从返回结果中获取重定向的request，如果重定向的request不为空的话，并且不超过重定向最大次数的话就进行重定向，否则返回结果。这里是通过一个while(true)的循环完成下一轮的重定向请求。
- **BridgeInterceptor(桥接拦截器)：** 对请求头以及返回结果处理  
    原理：负责将原始Requse转换给发送给服务端的Request以及将Response转化成对调用方需要的Response。具体就是对request添加Content-Type、Content-Length、cookie、Connection、Host、Accept-Encoding等请求头以及返回结果进行解压、保持cookie等。  
- **CacheInterceptor(缓存拦截器)：** 负责读取缓存以及更新缓存  
    原理：读取候选缓存cacheCandidate，根据originOequest和cacheresponse创建缓存策略CacheStrategy。根据缓存策略，来决定是否使用网络或者使用缓存或者返回错误。
具体的的缓存策略就是http的缓存策略。在结果返回阶段：负责将网络结果进行缓存。  
- **ConnectInterceptor(连接拦截器)：** 负责与服务器建立连接  
    使用StreamAllocation.newStream来和服务端建立连接，并返回输入输出流（HttpCodec），实际上是通过StreamAllocation中的findConnection寻找一个可用的Connection，然后调用Connection的connect方法，使用socket与服务端建立连接。  
- **CallServerInterceptor(网络拦截器)：** 负责从服务器读取相应的数据  
    主要的工作就是把请求的Request写入到服务端，然后从服务端读取Response。写入请求头、写入请求体、读取响应头、读取响应体。  

我们的网络请求就是这样经过责任链一级一级的递推下去，最终会执行到CallServerInterceptor的intercept方法，此方法会将网络响应的结果封装成一个Response对象并return。之后沿着责任链一级一级的回溯，最终就回到getResponseWithInterceptorChain方法的返回
- **应用拦截器和网络拦截器有什么区别？**  
从整个责任链路来看，应用拦截器是最先执行的拦截器，也就是用户自己设置request属性后的原始请求，而网络拦截器位于ConnectInterceptor和CallServerInterceptor之间，此时网络链路已经准备好，只等待发送请求数据。它们主要有以下区别。  
    - 首先，应用拦截器在RetryAndFollowUpInterceptor和CacheInterceptor之前，所以一旦发生错误重试或者网络重定向，网络拦截器可能执行多次，因为相当于进行了二次请求，但是应用拦截器永远只会触发一次。另外如果在CacheInterceptor中命中了缓存就不需要走网络请求了，因此会存在短路网络拦截器的情况。  
    - 其次，除了CallServerInterceptor之外，每个拦截器都应该至少调用一次realChain.proceed方法。实际上在应用拦截器这层可以多次调用proceed方法（本地异常重试）或者不调用proceed方法（中断），但是网络拦截器这层连接已经准备好，可且仅可调用一次proceed方法。  
    - 最后，从使用场景看，应用拦截器因为只会调用一次，通常用于统计客户端的网络请求发起情况；而网络拦截器一次调用代表了一定会发起一次网络通信，因此通常可用于统计网络链路上传输的数据。  

### OKHttp如何复用TCP链接
ConnectInterceptor的主要工作就是负责建立TCP链接，建立TCP链接需要经历三次握手四次挥手等操作，如果每个HTTP请求都要新建一个TCP消耗资源比较多。  
而Http1.1已经支持keep-alive，即多个Http请求复用一个TCP链接，OKHttp也做了相应的优化，下面我们来看下OKHttp是怎么复用TCP链接的。  
ConnectInterceptor中查找链接的代码会最终会调用到ExchangeFinder.findConnection方法  
首先会尝试使用 已给请求分配的链接。（已分配链接的情况例如重定向时的再次请求，说明上次已经有了链接）  
若没有 已分配的可用链接，就尝试从连接池中匹配获取。因为此时没有路由信息，所以匹配条件：address一致——host、port、代理等一致，且匹配的链接可以接受新的请求。  
若从链接池没有获取到，则传入routes再次尝试获取，这主要是针对Http2.0的一个操作，Http2.0可以复用square.com与square.ca的链接  
若第二次也没有获取到，就创建RealConnection实例，进行TCP + TLS握手，与服务端建立链接。  
此时为了确保Http2.0链接的多路复用性，会第三次从链接池匹配。因为新建立的链接的握手过程是非线程安全的，所以此时可能连接池新存入了相同的链接。  
第三次若匹配到，就使用已有链接，释放刚刚新建的链接；若未匹配到，则把新链接存入链接池并返回。  

### OKHttp空闲连接如何清除
在将连接加入连接池时就会启动定时任务。  
有空闲连接的话，如果最长的空闲时间大于5分钟或空闲数大于5，就移除关闭这个最长空闲连接；如果空闲数不大于5且最长的空闲时间不大于5分钟，就返回到5分钟的剩余时间，然后等待这个时间再来清理。  
没有空闲连接就等5分钟后再尝试清理。  
没有连接不清理。  

### OKHttp框架中用到了哪些设计模式
**责任链模式：** OKHttp的核心就是责任链模式，通过5个默认拦截器构成的责任链完成请求的配置。  
**享元模式：** 享元模式的核心即池中复用，OKHttp复用TCP连接时用到了连接池，同时在异步请求中也用到了线程池。

### Handler
Handler工作流程基本包括Handler、Looper、Message、MessageQueue 四个部分。
- handler的工作原理  
    1. Android中主线程是不能进行耗时操作的，子线程是不能进行更新UI的。所以就有了handler，它的作用就是实现线程之间的通信。
    2. handler整个流程中，主要有四个对象，handler，Message,MessageQueue,Looper。当应用  创建的时候，就会在主线程中创建handler对象，
    3. 我们通过要传送的消息保存到Message中，handler通过调用sendMessage方法将Message发送到MessageQueue中，Looper对象就会不断的调用loop()方法
    4. 不断的从MessageQueue中取出Message交给handler进行处理。从而实现线程之间的通信。
- Message  
    Message类就是定义了一个信息，这个信息中包含一个描述符和任意的数据对象，这个信息被用来传递给Handler.Message对象提供额外的两个int域和一个Object域。
    - Message的几个关键变量：
        - target：就是负责发送且分发它的Handler;
        - callback：是一个Runnable回调;
        - next：下一个消息池中的它之下的message。
- MessageQueue 消息队列  
    它持有并维护了当前线程的Looper分发的消息列表。消息并不是直接而是通过Handler 关联的Looper入队列的。  
    MessageQueue看上去是消息队列，然而它实际上只是消息队列的管理者。消息列表是它持有的Message mMessage，是由Message组成的单链表结构。
    - 核心方法：
        - boolean enqueueMessage(Message msg, long when) ；//消息入队列
        - Message next()。

- handler是如何分发到各个线程的
    1. 异步通信准备  
    在主线程创建Handler，则会直接在主线程中创建处理器对象Looper、消息队列对象MessageQueue和Handler对象。需要注意的是，Looper和MessageQueue均是属于创建线程的。Looper对象的创建一般通过Looper.prepareMainLooper()和Looper.prepare()两个方法，而创建你Looper对象的同时，将会自动创建MessageQueue，创建好MessageQueue后，Looper将自动进入消息循环。此时，Handler自动绑定了主线程Looper和MessageQueue。
    2. 消息入队  
    工作线程通过Handler发送消息Message到消息队列MessageQueue中，消息内容一般是UI 操作。发送消息一般都是通过Handler.sendMessage()和Handler.post()两个方法来进行的。而入队一般是通过MessageQueue.enqueueMessage()来处理。
    3. 消息循环  
    主要分为[消息出队]和[消息分发]两个步骤，Looper会通过循环取出消息队列MessageQueue里面的消息Message，并分发到创建该消息的处理者Handler。如果消息循环过程中，消息队列MessageQueue为空队列的话，则线程阻塞。
    4. 消息处理  
    Handler接收到Looper发来的信息，开始进行处理。  
 - 一般在开发中是怎么使用Handler的
    官方不允许在子线程中更新UI，所以我们经常会把需要更新UI的消息直接发给处理器Handler，通过重写Handler的handleMessage()方法进行UI的相关操作。
- Looper什么时候启动？Looper为什么不会阻塞主线程？  
    安卓是由事件驱动的，Looper.loop不断的接收处理事件，每一个点击触摸或者Activity每一个生命周期都是在Looper.loop的控制之下的，looper.loop一旦结束，应用程序的生命周期也就结束了。  
    思考一下发送ANR情况：事件没有得到处理；事件正在处理，但是没有及时完成。 对事件进行处理的就是looper，所以只能说事件的处理如果阻塞会导致ANR，looper的无限循环不会导致ANR。  
    而且主线程Looper从消息队列读取消息，当读完所有消息时，主线程阻塞。子线程往消息队列发送消息，并且往管道文件写数据，主线程即被唤醒，从管道文件读取数据，主线程被唤醒只是为了读取消息，当消息读取完毕，再次睡眠。因此loop的循环并不会对CPU性能有过多的消耗。  
    **可以说整个应用的生命周期都是在looper.loop()控制之下的(在应用启动的入口main函数中初始化ActivityThread，Handler，Looper，然后通过handler和looper去控制初始化应用)。而looper.loop采用的是Linux的管道机制，在没有消息的时候会进入阻塞状态，释放CPU执行权，等待被唤醒。真正会卡死主线程的操作是在回调方法onCreate/onStart/onResume等操作时间过长，会导致掉帧，甚至发生ANR，looper.loop本身不会导致应用卡死。**  
    **消息延迟的原理：** handler发送延迟消息，会将当前的延迟时间绑定到msg的when属性上，然后在循环MessageQUeue获取msg时判断如果当前有延迟就进行阻塞，通过计时器计算时间，时间通过系统启动计算时间，然后等待阻塞时间结束之后将其唤醒，在阻塞过程中会将之后的消息放在消息队列的头部去处理。
    **Handler机制如何保证消息不错乱：** handler机制中多个handler共有一个looper不会错乱是因为在handler 发送消息的时候，会将当前的handler对象绑定到message的target属性上，然后在Looper取到消息后通过msg.target拿到之前的handler对象，然后调用handler的handleMessage方法。  
    - **Handler内存泄漏原因以及解决办法**
        - **原因：**Handler造成内存泄露的原因。非静态内部类，或者匿名内部类。使得Handler默认持有外部类的引用。
在Activity销毁时，由于Handler可能有**未执行完/正在执行的Message**。导致Handler持有Activity的引用。
进而导致GC无法回收Activity。
        - **解决办法：** Activity销毁时，清空Handler中，未执行或正在执行的Callback以及Message。
    - **静态内部类+弱引用**  
        - **原因：** 
        我们想要一个对象被回收，那么前提它不被任何其它对象持有引用，所以当我们Activity页面关闭之后,  
        存在引用关系："未被处理/正处理的消息 -> Handler实例 -> 外部类"，如果在Handler消息队列 还有未处理的消息/正在处理消息时导致Activity不会被回收，从而造成内存泄漏。  
        - **解决方案：**  
            1. 将Handler的子类设置成静态内部类,使用弱引用持有Activity实例  
            2. 当外部类结束生命周期时，清空Handler内消息队列 

### 什么是线程池，如何使用
创建线程需要耗费资源和时间，如果任务来了才创建线程那么响应时间会变长，而且一个进程能创建的线程数有限。为了避免这些问题，在程序启动的时候就创建若干线程来响应处理，它们被称为线程池，里面的线程叫工作线程。比如单线程池，每次处理一个任务；数目固定的线程池或者是缓存线程池（一个适合很多生存期短的任务的程序的可扩展线程池）。
- 线程池的好处
    1. 重用存在的线程，减少对象创建、消亡的开销，性能佳
    2. 可有效控制最大并发线程数，提高系统资源的使用率，同时避免过多资源竞争，避免堵塞。
    3. 提供定时执行、定期执行、单线程、并发数控制等功能。  

    使用场景：比如说同时要下载多张图片,这个时候需要开线程,但是如果图片数量太多,每张图片图片都需要一个线程,那么就会造成cpu的销毁,所以这个时候就需要使用线程池来指定最大线程数量,其余需要下载的图片在线程池中等等下载.

- 四种线程池的使用场景，线程池的几个参数的理解
    Android中的线程池都是直接或间接通过配置ThreadPoolExecutor来实现不同特性的线程池Android中最常见的类具有不同特性的线程池分别为：
    1. newCachedThreadPoo:只有非核心线程，最大程数非常大,有线程都活动时会为新务创建新线程,否则会利用空闲线程(60s空闲时间,过了就会被回收 所以线程池中有0个线程的可能)来处理任务.
优点:任何任务都会被立即执行(任务队列SynchronousQuue相当于一个空集合);比较适合执行大量的耗时较少的任务.
    2. newFxedThreadPoo:只有核心线程，并且数量固定的，有线程都活动时，因为队列没有限制大小，新任会等待执行，当线程池空闲时不会释放工作线程，还会占用一定
的系统资源。
优点:更快的响应外界请求
    3. newScheduledThreadPool:核心线程数固定非核心线程(闲着没活会被立即回收数)没有限制
优点:执行定时任务以及有固定周期的重复任务
    4. newSingleThreadExecutor:只有一个核心线程确保所有的任务都在同一线程中按序完成
优点:不需要处理线程同步的问题  

    通过源码可以了解到上面的四种线程池实际上还是利用ThreadPoolExecutor类实现的  
    ![输入图片说明](android_images/imageb.png)  
    ![输入图片说明](android_images/imagec.png)  

### 如何实现线程同步
1. synchronized关键字修改的方法。
2. synchronized关键字修饰的语句块
3. 使用特殊域变量（volatile）实现线程同步

### 如何保证线程安全
1. synchronized;
2. Object方法中的wait,notify;
3. ThreadLocal机制;

### Android线程间通信
1. 调用Handler类
2. 调用Activity类的runOnUiThread方法
3. 调用View类中的post方法
4. 通过新建一个继承AsyncTask父类的子类来实现
5. 使用EventBus等工具

### Android进程间通信（英文缩写：IPC）
进程间通信 IPC为 (Inter-Process Communication) 缩写，称为进程间通信或跨进程通信，指两个进程间进行数据交换的过程。安卓中主要采用 Binder 进行进程间通信，当然也支持其他 IPC 方式，如：管道，Socket，文件共享，信号量等。
1. 使用 Bundle（Intent）
2. 使用文件共享
3. 使用 Messenger
4. 使用 AIDL
5. 使用 ContentProvider
6. 使用 Socket  
![输入图片说明](android_images/imaged.png)

### Java中几种常用设计模式
Java 中一般认为有23种设计模式，当然暂时不需要所有的都会，但是其中常见的几种设计模式应该去掌握。  
总体来说设计模式分为三大类：  
**创建型模式** 共五种：工厂方法模式、抽象工厂模式、单例模式、建造者模式、原型模式。  
**结构型模式** 共七种：适配器模式、装饰器模式、代理模式、外观模式、桥接模式、组合模式、享元模式。  
**行为型模式** 共十一种：策略模式、模板方法模式、观察者模式、迭代子模式、责任链模式、命令模式、备忘录模式、状态模式、访问者模式、中介者模式、解释器模式。

### 单例模式
所谓的单例设计指的是一个类只允许产生一个实例化对象。最好理解的一种设计模式，分为懒汉式和饿汉式。
- **饿汉式：** 构造方法私有化，外部无法产生新的实例化对象，只能通过static方法取得实例化对象
```java
class Singleton {
    /**
     * 在类的内部可以访问私有结构，所以可以在类的内部产生实例化对象
     */
    private static Singleton instance = new Singleton();
    /**
     * private 声明构造
     */
    private Singleton() {

    }
    /**
     * 返回对象实例
     */
    public static Singleton getInstance() {
        return instance;
    }

    public void print() {
        System.out.println("Hello Singleton...");
    }
}
```
- **懒汉式（双重校验）：** 当第一次去使用Singleton对象的时候才会为其产生实例化对象的操作  
```java
class Singleton {

    /**
     * 声明变量
     */
    private static volatile Singleton singleton = null;

    /**
     * 私有构造方法
     */
    private Singleton() {

    }

    /**
     * 提供对外方法
     * @return 
     */
    public static Singleton getInstance() {
        // 还未实例化
        if (singleton == null) {
            synchronized (Singleton.class) {
                if (singleton == null) {
                    singleton = new Singleton();
                }
            }
        }
        return singleton;
    }
    public void print() {
        System.out.println("Hello World");
    }
}
```  
当多个线程并发执行 getInstance 方法时，懒汉式会存在线程安全问题，所以用到了 synchronized 来实现线程的同步，当一个线程获得锁的时候其他线程就只能在外等待其执行完毕。而饿汉式则不存在线程安全的问题。

### 工厂设计模式
工厂模式分为工厂方法模式和抽象工厂模式。
- 工厂方法模式：
    1. 工厂方法模式分为三种：普通工厂模式，就是建立一个工厂类，对实现了同一接口的一些类进行实例的创建。
    2. 多个工厂方法模式，是对普通工厂方法模式的改进，在普通工厂方法模式中，如果传递的字符串出错，则不能正确创建对象，而多个工厂方法模式是提供多个工厂方法，分别创建对象。
    3. 静态工厂方法模式，将上面的多个工厂方法模式里的方法置为静态的，不需要创建实例，直接调用即可。
1. 普通工厂模式  
建立一个工厂类，对实现了同一接口的一些类进行实例的创建。  
```java
interface Sender {
    void Send();
}

class MailSender implements Sender {

    @Override
    public void Send() {
        System.out.println("This is mail sender...");
    }
}

class SmsSender implements Sender {

    @Override
    public void Send() {
        System.out.println("This is sms sender...");
    }
}

public class FactoryPattern {
    public static void main(String[] args) {
        Sender sender = produce("mail");
        sender.Send();
    }
    public static Sender produce(String str) {
        if ("mail".equals(str)) {
            return new MailSender();
        } else if ("sms".equals(str)) {
            return new SmsSender();
        } else {
            System.out.println("输入错误...");
            return null;
        }
    }
}
```

2. 多个工厂方法模式  
该模式是对普通工厂方法模式的改进，在普通工厂方法模式中，如果传递的字符串出错，则不能正确创建对象，而多个工厂方法模式是提供多个工厂方法，分别创建对象。
```java
interface Sender {
    void Send();
}

class MailSender implements Sender {

    @Override
    public void Send() {
        System.out.println("This is mail sender...");
    }
}

class SmsSender implements Sender {

    @Override
    public void Send() {
        System.out.println("This is sms sender...");
    }
}

class SendFactory {
    public Sender produceMail() {
        return new MailSender();
    }

    public Sender produceSms() {
        return new SmsSender();
    }
}

public class FactoryPattern {
    public static void main(String[] args) {
        SendFactory factory = new SendFactory();
        Sender sender = factory.produceMail();
        sender.Send();
    }
}
```
3. 静态工厂方法模式  
将上面的多个工厂方法模式里的方法置为静态的，不需要创建实例，直接调用即可。
```java
interface Sender {
    void Send();
}

class MailSender implements Sender {

    @Override
    public void Send() {
        System.out.println("This is mail sender...");
    }
}

class SmsSender implements Sender {

    @Override
    public void Send() {
        System.out.println("This is sms sender...");
    }
}

class SendFactory {
    public static Sender produceMail() {
        return new MailSender();
    }

    public static Sender produceSms() {
        return new SmsSender();
    }
}

public class FactoryPattern {
    public static void main(String[] args) {
        Sender sender = SendFactory.produceMail();
        sender.Send();
    }
}
```

### 抽象工厂模式
工厂方法模式有一个问题就是，类的创建依赖工厂类，也就是说，如果想要扩展程序，必须对工厂类进行修改，这违背了闭包原则，所以，从设计角度考虑，有一定的问题，如何解决？  
那么这就用到了抽象工厂模式，创建多个工厂类，这样一旦需要增加新的功能，直接增加新的工厂类就可以了，不需要修改之前的代码。  
```java
interface Provider {
    Sender produce();
}

interface Sender {
    void Send();
}

class MailSender implements Sender {

    public void Send() {
        System.out.println("This is mail sender...");
    }
}

class SmsSender implements Sender {

    public void Send() {
        System.out.println("This is sms sender...");
    }
}

class SendMailFactory implements Provider {

    public Sender produce() {
        return new MailSender();
    }
}

class SendSmsFactory implements Provider {

    public Sender produce() {
        return new SmsSender();
    }
}


public class FactoryPattern {
    public static void main(String[] args) {
        Provider provider = new SendMailFactory();
        Sender sender = provider.produce();
        sender.Send();
    }
}
```

### 建造者模式
工厂类模式提供的是创建单个类的模式，而建造者模式则是将各种产品集中起来管理，用来创建复合对象，所谓复合对象就是指某个类具有不同的属性。
```java
import java.util.ArrayList;
import java.util.List;

/**
 abstract class Builder {
    /**
     * 第一步：装CPU
     */
   public abstract void buildCPU();

    /**
     * 第二步：装主板
     */
    public abstract void buildMainBoard();

    /**
     * 第三步：装硬盘
     */
    public abstract void buildHD();

    /**
     * 获得组装好的电脑
     * @return
     */
    public abstract Computer getComputer();
}

/**
 * 装机人员装机
 */
class Director {
    public void Construct(Builder builder) {
        builder.buildCPU();
        builder.buildMainBoard();
        builder.buildHD();
    }
}

/**
 * 具体的装机人员
 */
class ConcreteBuilder extends  Builder {

    Computer computer = new Computer();

    @Override
    public void buildCPU() {
        computer.Add("装CPU");
    }

    @Override
    public void buildMainBoard() {
        computer.Add("装主板");
    }

    @Override
    public void buildHD() {
        computer.Add("装硬盘");
    }

    @Override
    public Computer getComputer() {
        return computer;
    }
}

class Computer {

    /**
     * 电脑组件集合
     */
    private List<String> parts = new ArrayList<String>();

    public void Add(String part) {
        parts.add(part);
    }

    public void print() {
        for (int i = 0; i < parts.size(); i++) {
            System.out.println("组件:" + parts.get(i) + "装好了...");
        }
        System.out.println("电脑组装完毕...");
    }
}

public class BuilderPattern {

    public static void main(String[] args) {
        Director director = new Director();
        Builder builder = new ConcreteBuilder();
        director.Construct(builder);
        Computer computer = builder.getComputer();
        computer.print();
    }
}
```

### 适配器设计模式
适配器模式是将某个类的接口转换成客户端期望的另一个接口表示，目的是消除由于接口不匹配所造成的的类的兼容性问题。主要分三类：类的适配器模式、对象的适配器模式、接口的适配器模式。
1. 类的适配器模式  
```java
class Source {
    public void method1() {
        System.out.println("This is original method...");
    }
}

interface Targetable {

    /**
     * 与原类中的方法相同
     */
    public void method1();

    /**
     * 新类的方法
     */
    public void method2();
}

class Adapter extends Source implements Targetable {

    @Override
    public void method2() {
        System.out.println("This is the targetable method...");
    }
}

public class AdapterPattern {
    public static void main(String[] args) {
        Targetable targetable = new Adapter();
        targetable.method1();
        targetable.method2();
    }
}
```
2. 对象的适配器模式  
基本思路和类的适配器模式相同，只是将Adapter 类作修改，这次不继承Source 类，而是持有Source 类的实例，以达到解决兼容性的问题。
```java
class Source {
    public void method1() {
        System.out.println("This is original method...");
    }
}

interface Targetable {

    /**
     * 与原类中的方法相同
     */
    public void method1();

    /**
     * 新类的方法
     */
    public void method2();
}

class Wrapper implements Targetable {

    private Source source;

    public Wrapper(Source source) {
        super();
        this.source = source;
    }

    @Override
    public void method1() {
        source.method1();
    }

    @Override
    public void method2() {
        System.out.println("This is the targetable method...");
    }
}

public class AdapterPattern {
    public static void main(String[] args) {
        Source source = new Source();
        Targetable targetable = new Wrapper(source);
        targetable.method1();
        targetable.method2();
    }
}
```
3. 接口的适配器模式  
接口的适配器是这样的：有时我们写的一个接口中有多个抽象方法，当我们写该接口的实现类时，必须实现该接口的所有方法，这明显有时比较浪费，因为并不是所有的方法都是我们需要的，有时只需要某一些，此处为了解决这个问题，我们引入了接口的适配器模式，借助于一个抽象类，该抽象类实现了该接口，实现了所有的方法，而我们不和原始的接口打交道，只和该抽象类取得联系，所以我们写一个类，继承该抽象类，重写我们需要的方法就行。
```java
/**
 * 定义端口接口，提供通信服务
 */
interface Port {
    /**
     * 远程SSH端口为22
     */
    void SSH();

    /**
     * 网络端口为80
     */
    void NET();

    /**
     * Tomcat容器端口为8080
     */
    void Tomcat();

    /**
     * MySQL数据库端口为3306
     */
    void MySQL();
}

/**
 * 定义抽象类实现端口接口，但是什么事情都不做
 */
abstract class Wrapper implements Port {
    @Override
    public void SSH() {

    }

    @Override
    public void NET() {

    }

    @Override
    public void Tomcat() {

    }

    @Override
    public void MySQL() {

    }
}

/**
 * 提供聊天服务
 * 需要网络功能
 */
class Chat extends Wrapper {
    @Override
    public void NET() {
        System.out.println("Hello World...");
    }
}

/**
 * 网站服务器
 * 需要Tomcat容器，Mysql数据库，网络服务，远程服务
 */
class Server extends Wrapper {
    @Override
    public void SSH() {
        System.out.println("Connect success...");
    }

    @Override
    public void NET() {
        System.out.println("WWW...");
    }

    @Override
    public void Tomcat() {
        System.out.println("Tomcat is running...");
    }

    @Override
    public void MySQL() {
        System.out.println("MySQL is running...");
    }
}

public class AdapterPattern {

    private static Port chatPort = new Chat();
    private static Port serverPort = new Server();

    public static void main(String[] args) {
        // 聊天服务
        chatPort.NET();

        // 服务器
        serverPort.SSH();
        serverPort.NET();
        serverPort.Tomcat();
        serverPort.MySQL();
    }
}
```

### 装饰模式
顾名思义，装饰模式就是给一个对象增加一些新的功能，而且是动态的，要求装饰对象和被装饰对象实现同一个接口，装饰对象持有被装饰对象的实例。
```java
interface Shape {
    void draw();
}

/**
 * 实现接口的实体类
 */
class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Rectangle...");
    }
}

class Circle implements Shape {

    @Override
    public void draw() {
        System.out.println("Shape: Circle...");
    }
}

/**
 * 创建实现了 Shape 接口的抽象装饰类。
 */
abstract class ShapeDecorator implements Shape {
    protected Shape decoratedShape;

    public ShapeDecorator(Shape decoratedShape) {
        this.decoratedShape = decoratedShape;
    }

    @Override
    public void draw() {
        decoratedShape.draw();
    }
}

/**
 *  创建扩展自 ShapeDecorator 类的实体装饰类。
 */
class RedShapeDecorator extends ShapeDecorator {

    public RedShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        setRedBorder(decoratedShape);
    }

    private void setRedBorder(Shape decoratedShape) {
        System.out.println("Border Color: Red");
    }
}

/**
 * 使用 RedShapeDecorator 来装饰 Shape 对象。
 */
public class DecoratorPattern {
    public static void main(String[] args) {
        Shape circle = new Circle();
        Shape redCircle = new RedShapeDecorator(new Circle());
        Shape redRectangle = new RedShapeDecorator(new Rectangle());
        System.out.println("Circle with normal border");
        circle.draw();

        System.out.println("\nCircle of red border");
        redCircle.draw();

        System.out.println("\nRectangle of red border");
        redRectangle.draw();
    }
}
```

### Android四大组件
- Activity
- Service
- Content Provider
- Broadcast Receiver

### Activity生命周期
- 正常情况下一个Activity会经历以下7个生命周期
    1. onCreate：当Activity第一次被运行时调用此方法，可用于加载布局视图，获取控件命名空间等一些初始化工作。
    2. onRestart：当Activity被重新启动的时候，调用此方法
    3. onStart ：表示Activity正在被启动，已经从不可见到可见状态（不是指用户可见，指Activity在后台运行，没有出现在前台），但还是无法与用户进行交互。
    4. onResume ：表Activity已经变为可见状态了，并且出现在前台工作了，也就是指用户可见了
    5. onPause ：表示Activity正在暂停，但Activity依然可见，可以执行一些轻量级操作，但一般不会进行太多操作，因为这样会影响用户体验。
    6. onStop ：表示Activity即将暂停，此时Activity工作在后台，已经不可见了，可以与onPause方法一样做一些轻量级操作，但依然不能太耗时。
    7. onDestroy ：表示活动即将被销毁。  
    ![输入图片说明](android_images/imagee.png)  
    **onStart与onResume，onPause与onStop之间的差距：** 前者呢是关于Activity是否可见，后者呢是关于Activity是否位于前台
- 异常情况下Activity的生命周期，异常情况常见的大概分为两种  
    1. **系统配置发生改变：** 这一种情况，最常见的就是屏幕发生旋转，导致Activity被杀死而重新创建，下面这一张图片能够充分体现。  
当发生这一异常时，先调用onPause，然后调用onSaveInstanceState()方法保存数据，然后重新创建Activity，然后调用onRestoreInstanceState()把之前Activity的数据恢复.  
    ![输入图片说明](android_images/imagef.png)
    2. **资源内存不够：** 这一种情况的数据保存与恢复和前面一样。但杀死Activity的优先级分为以下三种：
        - Activity处于前台——用户可见并且能进行交互，优先级最高
        - Activity处于非前台——Activity可见但用户无法进行交互()
        - Activity处于后台——不可见也无法与用户进行交互，比如执行了onStop（），优先级最低。

### Activity的四种启动模式及应用场景
1. standard标准模式：
每次激活Activity时都会创建Activity实例，并放入任务栈中。使	用场景：大多数Activity。
此模式的Activity默认会进入启动它的Activity所属的任务栈中；
2. singleTop栈顶复用模式：
如果在任务的栈顶正好存在该Activity的实例，就重用该实例( 会调用实例的 onNewIntent() )，否则就会创建新的实例并放入栈顶，即使栈中已经存在该Activity	的实例，只要不在栈顶，都会创建新的实例。使用场景如新闻类或者阅读类App的内容页面。
3. singleTask栈内复用模式：
如果在栈中已经有该Activity的实例，就重用该实例(会调用实例的 onNewIntent() )。重用时，会让该实例回到栈顶，因此在它上面的实例将会被移出栈。如果栈中不存在该实例，将会创建新的实例放入栈中。使用场景如浏览器的主界面。不管从多少个应用启动浏览器，只会启动主界面一次，其余情况都会走onNewIntent，并且会清空主界面上面的其他页面。  
4. singleInstance单实例模式：
在一个新栈中创建该Activity的实例，并让多个应用共享该栈中的该Activity实例。一旦该模式的Activity实例已经存在于某个栈中，任何应用再激活该Activity时都会重用该栈中的实例( 会调用实例的 onNewIntent() )。其效果相当于多个应用共	享一个应用，不管谁激活该 Activity 都会进入同一个应用中。使用场景如闹铃提醒，将闹铃提醒与闹铃设置分离。singleInstance不要用于中间页面，如果用于中间页面，跳转会有问题，比如：A -> B (singleInstance) -> C，完全退出后，在此启动，首先打开的是B。

### Fragment生命周期
Fragment必须是依存与Activity而存在的，因此Activity的生命周期会直接影响到Fragment的生命周期。Fragment状态与Activity类似，也存在如下4种状态：
- 运行：当前Fmgment位于前台，用户可见，可以获得焦点。
- 暂停：其他Activity位于前台，该Fragment依然可见，只是不能获得焦点。
- 停止：该Fragment不可见，失去焦点。
- 销毁：该Fragment被完全删除，或该Fragment所在的Activity被结束。
Fragment的生命周期与Activity的生命周期十分相似，如下图所示：  
![输入图片说明](android_images/imageg.png)  
从上图可以看出，Activity中的生命周期方法，Fragment中基本都有，但是Fragment比Activity多几个方法。
各生命周期方法的含义如下：
    - onAttach() ：当Fragment与Activity发生关联时调用。
    - onCreate()：创建Fragment时被回调。
    - onCreateView()：每次创建、绘制该Fragment的View组件时回调该方法，Fragment将会显示该方法返回的View 组件。
    - onActivityCreated()：当 Fragment 所在的Activity被启动完成后回调该方法。
    - onStart()：启动 Fragment 时被回调，此时Fragment可见。
    - onResume()：恢复 Fragment 时被回调，获取焦点时回调。
    - onPause()：暂停 Fragment 时被回调，失去焦点时回调。
    - onStop()：- 停止 Fragment 时被回调，Fragment不可见时回调。
    - onDestroyView()：销毁与Fragment有关的视图，但未与Activity解除绑定。
    - onDestroy()：销毁 Fragment 时被回调。
    - onDetach()：与onAttach相对应，当Fragment与Activity关联被取消时调用。
- 生命周期调用
    1. 创建Fragment：  
    onAttach() —> onCreate() —> onCreateView() —> onActivityCreated() —> onStart() —> onResume()
    2. 按下Home键回到桌面 / 锁屏：  
    onPause() —> onStop()
    3. 从桌面回到Fragment / 解锁：  
    onStart() —> onResume()
    4. 切换到其他Fragment：  
    onPause() —> onStop() —> onDestroyView()
    5. 切换回本身的Fragment：  
    onCreateView() —> onActivityCreated() —> onStart() —> onResume()
    6. 按下Back键退出：  
    onPause() —> onStop() —> onDestroyView() —> onDestroy() —> onDetach()  

**结语**  
Fragment的生命周期介绍完毕。Fragment具有与Activity很相似的生命周期，依存与Activity而存在的，因此Activity的生命周期会直接影响到Fragment的生命周期。

### Android Service两种启动方式
下图中左边是StartService的生命周期，右边是bindService的生命周期  
![Service生命周期](android_images/imageh.png)

- 第一种方式：通过StartService启动Service
通过startService启动后，service会一直无限期运行下去，只有外部调用了stopService()或stopSelf()方法时，该Service才会停止运行并销毁。  
要创建一个这样的Service，你需要让该类继承Service类，然后重写以下方法：
    - onCreate()
        1. 如果service没被创建过，调用startService()后会执行onCreate()回调；
        2. 如果service已处于运行中，调用startService()不会执行onCreate()方法。

        也就是说，onCreate()只会在第一次创建service时候调用，多次执行startService()不会重复调用onCreate()，此方法适合完成一些初始化工作。
    - onStartCommand()  
    如果多次执行了Context的startService()方法，那么Service的onStartCommand()方法也会相应的多次调用。onStartCommand()方法很重要，我们在该方法中根据传入的Intent参数进行实际的操作，比如会在此处创建一个线程用于下载数据或播放音乐等。
    - onBind()  
    Service中的onBind()方法是抽象方法，Service类本身就是抽象类，所以onBind()方法是必须重写的，即使我们用不到。
    - onDestory()  
    在销毁的时候会执行Service该方法。  

    这几个方法都是回调方法，且在主线程中执行，由android操作系统在合适的时机调用。

- 第二种方式：通过bindService启动Service  
bindService启动服务特点：  
    1. bindService启动的服务和调用者之间是典型的client-server模式。调用者是client，service则是server端。service只有一个，但绑定到service上面的client可以有一个或很多个。这里所提到的client指的是组件，比如某个Activity。
    2. client可以通过IBinder接口获取Service实例，从而实现在client端直接调用Service中的方法以实现灵活交互，这在通过startService方法启动中是无法实现的。
    3. bindService启动服务的生命周期与其绑定的client息息相关。当client销毁时，client会自动与Service解除绑定。当然，client也可以明确调用Context的unbindService()方法与Service解除绑定。当没有任何client与Service绑定时，Service会自行销毁。

### ContentProvider（内容提供程序）
[**Google官方的ContentProvider文档**](https://developer.android.google.cn/guide/topics/providers/content-providers?hl=zh-cn) 
 
内容提供程序有助于应用管理其自身和其他应用所存储数据的访问，并提供与其他应用共享数据的方法。它们会封装数据，并提供用于定义数据安全性的机制。内容提供程序是一种标准接口，可将一个进程中的数据与另一个进程中运行的代码进行连。实现内容提供程序大有好处。最重要的是，通过配置内容提供程序，您可以使其他应用安全地访问和修改您的应用数据（如图 1 所示）。  
![输入图片说明](android_images/imagei.png)  
图 1. 内容提供程序如何管理存储空间访问的概览图。  

如果您计划共享数据，则可使用内容提供程序。如果您不打算共享数据，也可使用内容提供程序，因为它们可以提供很好的抽象，但无需如此。此抽象可让您修改应用数据存储实现，同时不会影响依赖数据访问的其他现有应用。在此情况下，受影响的只有您的内容提供程序，而非访问该提供程序的应用。例如，您可以将 SQLite 数据库换成其他存储空间（如图 2 所示）。  
![输入图片说明](android_images/imagej.png)  
图 2. 迁移内容提供程序存储空间的图示意图  

许多其他类依赖于 ContentProvider 类：
- AbstractThreadedSyncAdapter
- CursorAdapter
- CursorLoader

如果您正在使用以上某个类，则还需在应用中实现内容提供程序。请注意，使用同步适配器框架时，您还可使用另一种方案：创建存根内容提供程序。如需了解有关此主题的更多信息，请参阅创建存根内容提供程序。此外，在下列情况下，您需要自定义内容提供程序：
- 您希望在自己的应用中实现自定义搜索建议
- 您需要使用内容提供程序向微件公开应用数据
- 您希望将自己应用内的复杂数据或文件复制并粘贴到其他应用中  

Android 框架内的某些内容提供程序可管理音频、视频、图像和个人联系信息等数据。android.provider 软件包参考文档中列出了其中的部分提供程序。虽然存在一些限制，但任何 Android 应用均可访问这些提供程序。

内容提供程序可用于管理对各种数据存储源的访问，包括结构化数据（如 SQLite 关系型数据库）和非结构化数据（如图像文件）。如需了解 Android 可用存储类型的详细信息，请参阅存储选项和设计数据存储。

**内容提供程序的优点**  
内容提供程序可精细控制数据访问权限。您可以选择仅在应用内限制对内容提供程序的访问，授予访问其他应用数据的权限，或配置读取和写入数据的不同权限。如需了解有关安全使用内容提供程序的更多信息，请参阅存储数据的安全提示与内容提供程序权限。

您可以使用内容提供程序将细节抽象化，以用于访问应用中的不同数据源。例如，您的应用可能会在 SQLite 数据库中存储结构化记录，以及视频和音频文件。如果您在应用中实现此开发模式，则可使用内容提供程序访问所有这类数据。

另请注意，CursorLoader 对象依赖于内容提供程序来运行异步查询，进而将结果返回至应用的界面层。如需了解有关使用 CursorLoader 在后台加载数据的更多信息，请参阅使用 CursorLoader 运行查询。

以下主题详细描述了内容提供程序：  
**内容提供程序基础知识**  
如何使用现有内容提供程序访问和更新数据。  
**创建内容提供程序**  
如何设计并实现自己的内容提供程序。  
**日历提供程序**  
如何访问 Android 平台的日历提供程序。  
**联系人提供程序**  
如何访问 Android 平台的联系人提供程序。

### Broadcast（广播）
Android应用可以通过广播从系统或其他App接收或发送消息。类似于订阅-发布设计模式。当某些事件发生时，可以发出广播。 系统在某些状态改变时会发出广播，例如开机、充电。App也可发送自定义广播。广播可用于应用间的通讯，是IPC的一种方式。
- 广播的种类（也可以看成是广播的属性）：
    - 标准广播（Normal Broadcasts） 完全异步的广播。广播发出后，所有的广播接收器几乎同时接收到这条广播。 不同的App可以注册并接到标准广播。例如系统广播。
    - 有序广播（Ordered Broadcasts） 同步广播。同一时刻只有一个广播接收器能接收到这条广播。这个接收器处理完后，广播才会继续传递。 有序广播是全局的广播。
    - 本地广播（Local Broaddcasts） 只在本App发送和接收的广播。注册为本地广播的接收器无法收到标准广播。
    - 带权限的广播 发送广播时可以带上相关权限，申请了权限的 App 或广播接收器才能收到相应的带权限的广播。 如果在 manifest 中申请了相应权限，接收器可以不用再申请一次权限即可接到相应广播。
- 接收广播  
创建广播接收器，调用onReceive()方法，需要一个继承 BroadcastReceiver 的类。
- 注册广播  
代码中注册称为动态注册。在AndroidManifest.xml中注册称为静态注册。动态注册的刚波接收器一定要取消注册。在onDestroy()方法中调用unregisterReceiver()方法来取消注册。

不要在onReceive()方法中添加过多的逻辑操作或耗时的操作。因为在广播接收器中不允许开启线程，当onReceive()方法运行较长时间而没结束时，程序会报错。因此广播接收器一般用来打开其他组件，比如创建一条状态栏通知或启动一个服务。  
- 发送广播
App有3种发送广播的方式。发送广播需要使用Intent类。
    - sendOrderedBroadcast(Intent, String)  
    发送有序广播。每次只有1个广播接收器能接到广播。 接收器接到有序广播后，可以完全地截断广播，或者传递一些信息给下一个接收器。 有序广播的顺序可受android:priority标签影响。同等级的接收器收到广播的顺序是随机的。
    - sendBroadcast(Intent)  
    以一个未定义的顺序向所有接收器发送广播。也称作普通广播。 这种方式更高效，但是接收器不能给下一个接收器传递消息。这类广播也无法截断。
    - LocalBroadcastManager.sendBroadcast  
    广播只能在应用程序内部进行传递，并且广播接收器也只能接收到来自本应用程序发出的广播。 这个方法比全局广播更高效（不需要Interprocess communication，IPC），而且不需要担心其它App会收到你的广播以及其他安全问题。

- 广播与权限  
    - 发送带着权限的广播  
    当你调用sendBroadcast(Intent, String)或sendOrderedBroadcast(Intent, String, BroadcastReceiver, Handler, int, String, Bundle)时，你可以指定一个权限。 接收器在manifest中申请了相应权限时才能收到这个广播。
        - 以SEND_SMS权限为例：
        ```java
        //例如发送一个带着权限的广播
        sendBroadcast(new Intent("com.example.NOTIFY"), Manifest.permission.SEND_SMS);
        ```
        ```xml
        <!-- 接收广播的app必须注册相应的权限 -->
        <uses-permission android:name="android.permission.SEND_SMS"/>
        ```
        - 以自定义权限为例：
        ```xml
        <!-- 当然也可以使用自定义。在 manifest 中使用 permission 标签 -->
        <permission android:name="custom_permission" />
        ```
        ```java
        // 添加后编译一下。即可调用
        Manifest.permission.custom_permission
        ```
- 接收带权限的广播  
    若注册广播接收器时申明了权限，那么只会接收到带着相应权限的广播。  
    在配置文件中声明权限，程序才能访问一些关键信息。 例如允许查询系统网络状态。  
    ```xml
        <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
        ​<!-- 机器开机广播 -->
        <uses-permission android:name="android.permission.BOOT_COMPLETED">
    ```
如果没有申请权限，程序可能会意外关闭。

### View的绘制流程
OnMeasure()——>OnLayout()——>OnDraw()  
1. OnMeasure()：  
测量视图大小。从顶层父View到子View递归调用measure方法，measure 调用链最终会回调 View/ViewGroup 对象的 onMeasure()方法，因此自定义视图时，只需要复写 onMeasure() 方法即可。  
2. OnLayout()：  
确定View位置，进行页面布局。从顶层父View向子View的递归调用 view.layout方法的过程，即父View根据上一步measure子View所得到的布局大小和布局参数，将子View放在合适的位置上。  
3. OnDraw()：  
绘制视图。ViewRoot创建一个Canvas对象，然后调用OnDraw()。六个步骤：
    1. 绘制视图的背景
    2. 保存画布的图层（Layer）
    3. 绘制View的内容
    4. 绘制View子视图，如果没有就不用
    5. 还原图层（Layer）
    6. 绘制滚动条

### Android的三种动画
**帧动画、View动画（补间动画）、属性动画**  
- 帧动画
    帧动画就是顺序播放一组预先定义好的图片，就类似于我们观看视频，就是一张一张的图片连续播放。
    帧动画的使用很简单，总共就两个步骤：
    1. 在res/drawable目录下定义一个XML文件，根节点为系统提供的animation-list，然后放入定义更好的图片；
    2. 使用AnimationDrawable类播放第一步定义好的Drawable中的图片，形成动画效果；
- View动画（补间动画）  
    view动画也称为补间动画，因为我们只需要拿到一个view，设定它开始和结束的位置，中间的view会自动由系统补齐，而不需要帧动画每一幅图都是提前准备好的。
    View动画是Android一开始就提供的比较原始的动画，主要支持四种效果：**平移、缩放、旋转、透明度变化（渐变） 四种基本效果**，我们可以再这四种基础效果的基础上，选择其中的几种进行组合。
    View动画的四种基本效果对应了四个Animation的子类，如下：  
    ![输入图片说明](android_images/imagek.png)  
    View动画的组合动画–AnimationSet  
    我们可以使用AnimationSet把View动画的平移、缩放、旋转、渐变都揉在一起，也是既能通过代码实现，也可以通过xml实现
- 属性动画  
    属性动画可以看作是增强版的补间动画，与补间动画的不同之处体现在：  
    - 补间动画只能定义两个关键帧在透明、旋转、位移和倾斜这四个属性的变换，但是属性动画可以定义任何属性的变化。
    - 补间动画只能对 UI 组件执行动画，但属性动画可以对任何对象执行动画。  

    与补间动画类似的是，属性动画也需要定义几个方面的属性：  
    - 动画持续时间。默认为 300ms，可以通过 android:duration 属性指定。
    - 动画插值方式。通过 android:interploator 指定。
    - 动画重复次数。通过 android:repeatCount 指定。
    - 重复行为。通过 android:repeatMode 指定。
    - 动画集。在属性资源文件中通过 <set …/> 来组合。
    - 帧刷新率。指定多长时间播放一帧。默认为 10 ms。

### Glide 缓存机制
- 为什么要缓存？
    1. 减少流量消耗，加快响应速度；
    2. Bitmap 的创建／销毁比较耗内存，可能会导致频繁GC；使用缓存可以更加高效地加载 Bitmap，减少卡顿。
- Glide的缓存结构
    - 内存缓存
        其中内存缓存又分为2种
        - 弱引用（WeakReference）缓存，正在使用的资源缓存
        - Lrucache 缓存
    - 硬盘缓存  
    磁盘缓存就是DiskLrucache
所以现在看起来Glide3级缓存的话应该是：弱引用 + Lrucache + DiskLrucache。
### HashMap和Hashtable的区别
- 相同点  
都实现了map、Cloneable（可克隆）、Serializable（可序列化）这三个接口
- 不同点
    1. Hashtable 是不允许键或值为 null 的，HashMap 的键值则都可以为 null。
    2. 实现方式不同：Hashtable 继承的是 Dictionary类，而 HashMap 继承的是 AbstractMap 类
    3. Hashtable是线程安全的，所以在单线程环境下它比HashMap要慢。
    4. HashMap不是同步的，适用于单线程环境。
所以在单线程环境下Hashtable比HashMap要慢。如果不需要同步，只需要单一线程，那么使用HashMap性能要好过Hashtable。

### ArrayList和LinkedList区别
1. ArrayList的实现是基于数组，LinkedList的实现是基于双向链表。
2. 对于随机访问，ArrayList优于LinkedList
3. 对于插入和删除操作，LinkedList优于ArrayList
4. LinkedList比ArrayList更占内存，因为LinkedList的节点除了存储数据，还存储了两个引用，一个指向前一个元素，一个指向后一个元素。
### ANR出现的原因及解决方法
- 简介  
    ANR全称：Application Not Responding，也就是应用程序无响应。
- 原因  
    Android系统中，ActivityManagerService(简称AMS)和WindowManagerService(简称WMS)会检测App的响应时间，如果App在特定时间无法相应屏幕触摸或键盘输入时间，或者特定事件没有处理完毕，就会出现ANR。
    - 以下四个条件都可以造成ANR发生：
        - InputDispatching Timeout：5秒内无法响应屏幕触摸事件或键盘输入事件
        - BroadcastQueue Timeout ：在执行前台广播（BroadcastReceiver）的onReceive()函数时10秒没有处理完成，后台为60秒。
        - Service Timeout ：前台服务20秒内，后台服务在200秒内没有执行完毕。
        - ContentProvider Timeout ：ContentProvider的publish在10s内没进行完。
- 避免  
    尽量避免在主线程（UI线程）中作耗时操作。
- ANR分析办法  
    分析traces.txt（'/data/anr/traces.txt'）日志文件，使用adb命令把traces.txt文件从手机里导出来。
### JAVA的堆栈
- 堆内存  
    - 什么是堆内存？  
        堆内存是java内存中的一种，它的作用是用于存储java中的对象和数组，当我们new一个对象或者创建一个数组的时候，就会在堆内存中开辟一段空间给它，用于存放。  
    - 堆内存的特点是什么？  
        1. 堆其实可以类似的看做是管道，或者说是平时去排队买票的情况差不多，所以堆内存的特点就是：先进先出，后进后出，也就是你先排队好，你先买票。  
        2. 堆可以动态地分配内存大小，生存期也不必事先告诉编译器，因为它是在运行时动态分配内存的，但缺点是，由于要在运行时动态分配内存，存取速度较慢。  
    - new对象在堆中如何分配？  
        由Java虚拟机的自动垃圾回收器来管理。
- 栈内存  
    - 什么是栈内存？  
        栈内存是Java的另一种内存，主要是用来执行程序用的，比如：基本类型的变量和对象的引用变量
    - 栈内存的特点
        1. 栈内存就好像一个矿泉水瓶，往里面放入东西，那马先放入的沉入底部，所以它的特点是：先进后出，后进先出
        2. 存取速度比堆要快，仅次于寄存器，栈数据可以共享，但缺点是，存在栈中的数据大小与生存必须是确定的，缺乏灵活性
    - 栈内存分配机制  
        栈内存可以称为一级缓存，由垃圾回收器自动回收
- 栈和堆的区别  
    JVM是基于堆栈的虚拟机，JVM为新创建的线程都分配一个堆栈，也就是说，对于一个Java程序来说，它的运行就是通过对堆栈的操作来完成的。堆栈以帧为单位保存线程的状态。JVM对堆栈只进行两种操作：以帧为单位的压栈和出栈操作。
    - 差异：
        1. 堆内存用来存放由new创建的对象和数组
        2. 栈内存用来存放方法或者局部变量等
        3. 堆是先进先出，后进后出
        4. 栈是先进后出，后进先出
        5. 共享性的不同：
        6. 栈内存是线程私有的，堆内存是所有线程共有的

### WebView如何与前端网页(JavaScript)交互
- [**WebView与 JS 交互方式**](http://https://blog.51cto.com/u_15762357/5948558)

### Android WebView内存泄漏
- 要解决 WebView 内存泄漏的要点如下：  
    1. 不要使用 xml 方式创建，而是使用代码把 WebView 给 new 出来  
    2. 不要让 WebView 持有对 Activity/Fragment 的 Context 引用（核心）
    3. 销毁时，停止 WebView 的加载，并从父控件中将其移除
1. 初始化 WebView  
    WebView 内存泄露的主要原因是引用了 Activity/Fragment 的 Context，加之 WebView 本身的设计问题，导致 Activity/Fragment 无法被即时释放，既然 WebView 无法即时释放 Context，那就让它引用全局的 Context 就好了：
    ```dart
    // 让 WebView 使用 ApplicationContext
    val webview = WebView(this.applicationContext)
    ```
    > 切记：不要在 xml 中使用 <WebView> 标签创建 WebView
2. 销毁 WebView  
    为了保证 Activity/Fragment 界面销毁时，WebView 不要在后台继续进行没有意义的加载，并且避免父控件对 WebView 的引用导致发生意外泄露，需要进行如下 2 步：  
    - 将 WebView 从其父控件中移除  
    - 让 WebView 停止加载页面并释放 
    ```kotlin
    override fun onDestroy() {
        // webview?.loadDataWithBaseURL(null, "", "text/html", "utf-8", null)
        // webview?.clearView()
        webview?.loadUrl("about:blank")
        webview?.parent?.let {
            (it as ViewGroup).removeView(webview)
        }
        webview?.stopLoading()
        webview?.settings?.javaScriptEnabled = false
        webview?.clearHistory()
        webview?.clearCache(true)
        webview?.removeAllViewsInLayout()
        webview?.removeAllViews()
        webview?.webViewClient = null
        webview?.webChromeClient = null
        webview?.destroy()
        webview = null
        super.onDestroy()
    }
    
    ```
### Android事件分发机制
**摘要:** 在Android开发中，事件分发机制是非常重要的一个环节，它负责处理用户触摸、点击等交互行为。本文将详细讲解Android事件分发机制的基本概念、原理和实践，为开发者提供一个更深入的理解。  
- 事件分发基本概念
    1. 事件类型  
    在Android中，常见的事件类型有三种：ACTION_DOWN、ACTION_MOVE、ACTION_UP。分别表示触摸按下、触摸移动和触摸抬起。
    2. 事件分发的对象  
    Android事件分发的对象主要包括Activity、ViewGroup、View三个层级。
    3. 事件分发的主要方法  
    事件分发的主要方法有三个：dispatchTouchEvent()、onInterceptTouchEvent()和onTouchEvent()。这三个方法在不同层级之间进行调用和相互协作，共同完成事件分发。
- 事件分发原理
    1. 事件分发流程  
    事件分发的流程遵循以下原则：
        * 从Activity开始，事件按照从上到下的顺序依次传递给ViewGroup和View。
        * 如果事件被消费，则传递结束；否则，事件会按照从下到上的顺序回溯到Activity，寻找合适的消费者。
    2. 三个方法的作用
        * dispatchTouchEvent()：负责分发事件，通常不需要重写。
        * onInterceptTouchEvent()：仅存在于ViewGroup中，用于判断是否拦截事件。默认返回false，即不拦截。
        * onTouchEvent()：负责处理触摸事件，返回true表示消费事件，返回false表示不消费。
- 实践案例  
下面通过一个简单的案例来说明事件分发机制的应用。  
假设我们有一个自定义的ViewGroup（MyViewGroup）和一个自定义的View（MyView），需要在MyView中处理点击事件，而MyViewGroup需要在滑动时拦截事件。
    1. MyViewGroup的处理  
    在MyViewGroup中，我们需要重写onInterceptTouchEvent()方法，根据触摸事件类型判断是否拦截：
        ```kotlin
        @Override
        public boolean onInterceptTouchEvent(MotionEvent ev) {
            int action = ev.getAction();
            switch (action) {
                case MotionEvent.ACTION_MOVE:
                    return true; // 拦截滑动事件
                default:
                    return super.onInterceptTouchEvent(ev);
            }
        }
        ```
    2. MyView的处理  
    在MyView中，我们需要重写onTouchEvent()方法，处理点击事件：
        ```kotlin
        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    // 处理按下事件
                    break;
                case MotionEvent.ACTION_MOVE:
                    // 处理移动事件
                    break;
                case MotionEvent.ACTION_UP:
                    // 处理抬起事件
                break;
            default:
                return super.onTouchEvent(event);
        }
        return true; // 表示消费事件 
        ```
- 注意事项  
在实际开发中，我们需要注意以下几点：
    1. 事件分发优先级  
    事件分发的优先级是：Activity > ViewGroup >View。事件先由Activity开始分发，然后依次传递到ViewGroup和View。
    2. 事件消费  
    一旦某个事件被消费，后续事件将直接发送到消费者，而不再经过其他层级。例如，如果MyView消费了ACTION_DOWN事件，那么接下来的ACTION_MOVE和ACTION_UP事件也会直接发送到MyView。
    3. 事件拦截  
    当ViewGroup拦截了某个事件，该事件将不再传递给其子View。需要注意的是，拦截事件并不意味着消费事件，只是改变了事件的传递路径。在上面的案例中，MyViewGroup拦截了滑动事件，但并没有消费，因此事件将继续沿着事件分发链回溯。
- 总结  
Android事件分发机制是一个相对复杂的系统，通过理解其基本概念、原理和实践，我们可以更好地处理用户交互事件。在开发过程中，我们需要熟悉各种事件类型，了解事件分发的优先级和消费规则，以便根据实际需求灵活应用。

### ViewModel在屏幕旋转时的生命周期
[**Jetpack生命周期感知组件ViewModel**](https://juejin.cn/post/7283018190593507368)

### Fragment懒加载
