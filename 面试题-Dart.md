# Dart 面试题

### Dart 当中的 「..」表示什么意思？ 
Dart 当中的 「..」意思是 「级联操作符」，为了方便配置而使用。「..」和「.」不同的是 调用「..」后返回的相当于是 this，而「.」返回的则是该方法返回的值

### Dart 的作用域
Dart 没有 「public」「private」等关键字，默认就是公开的，私有变量使用 下划线 _开头。

### Dart 是不是单线程模型？是如何运行的？
Dart 是单线程模型，运行的的流程如下图。  
![输入图片说明](dart_images/darta.jpg)

简单来说，Dart 在单线程中是以消息循环机制来运行的，包含两个任务队列，一个是“微任务队列” microtask queue，另一个叫做“事件队列” event queue。  
当Flutter应用启动后，消息循环机制便启动了。首先会按照先进先出的顺序逐个执行微任务队列中的任务，当所有微任务队列执行完后便开始执行事件队列中的任务，事件任务执行完毕后再去执行微任务，如此循环往复，生生不息。

### Dart 是如何实现多任务并行的？
前面说过， Dart 是单线程的，不存在多线程，那如何进行多任务并行的呢？其实，Dart的多线程和前端的多线程有很多的相似之处。Flutter的多线程主要依赖Dart的并发编程、异步和事件驱动机制。  
![输入图片说明](dart_images/dartb.png)  
简单的说，在Dart中，一个Isolate对象其实就是一个isolate执行环境的引用，一般来说我们都是通过当前的isolate去控制其他的isolate完成彼此之间的交互，而当我们想要创建一个新的Isolate可以使用Isolate.spawn方法获取返回的一个新的isolate对象，两个isolate之间使用SendPort相互发送消息，而isolate中也存在了一个与之对应的ReceivePort接受消息用来处理，但是我们需要注意的是，ReceivePort和SendPort在每个isolate都有一对，只有同一个isolate中的ReceivePort才能接受到当前类的SendPort发送的消息并且处理。

### 说一下Dart异步编程中的 Future关键字
前面说过，Dart 在单线程中是以消息循环机制来运行的，其中包含两个任务队列，一个是“微任务队列” microtask queue，另一个叫做“事件队列” event queue。  
在Java并发编程开发中，经常会使用Future来处理异步或者延迟处理任务等操作。而在Dart中，执行一个异步任务同样也可以使用Future来处理。在 Dart 的每一个 Isolate 当中，执行的优先级为 ：Main > MicroTask > EventQueue。

### 说一下Dart异步编程中的 Stream数据流
在Dart中，Stream 和 Future 一样，都是用来处理异步编程的工具。它们的区别在于，Stream 可以接收多个异步结果，而Future 只有一个。 Stream 的创建可以使用 Stream.fromFuture，也可以使用 StreamController 来创建和控制。还有一个注意点是：普通的 Stream 只可以有一个订阅者，如果想要多订阅的话，要使用 asBroadcastStream()。

### Stream 有哪两种订阅模式？分别是怎么调用的？ 
Stream有两种订阅模式：单订阅(single) 和 多订阅（broadcast）。单订阅就是只能有一个订阅者，而广播是可以有多个订阅者。这就有点类似于消息服务（Message Service）的处理模式。单订阅类似于点对点，在订阅者出现之前会持有数据，在订阅者出现之后就才转交给它。而广播类似于发布订阅模式，可以同时有多个订阅者，当有数据时就会传递给所有的订阅者，而不管当前是否已有订阅者存在。  
Stream 默认处于单订阅模式，所以同一个 stream 上的 listen 和其它大多数方法只能调用一次，调用第二次就会报错。但 Stream 可以通过 transform() 方法（返回另一个 Stream）进行连续调用。通过 Stream.asBroadcastStream() 可以将一个单订阅模式的 Stream 转换成一个多订阅模式的 Stream，isBroadcast 属性可以判断当前 Stream 所处的模式。

### await for 如何使用？ 
await for是不断获取stream流中的数据，然后执行循环体中的操作。它一般用在直到stream什么时候完成，并且必须等待传递完成之后才能使用，不然就会一直阻塞。
示例代码：
```dart
Stream<String> stream = new Stream<String>.fromIterable(['不开心', '面试', '没', '过']);
main() async{
    await for(String s in stream){
    print(s);
  }
}
```

###  说一下 mixin机制
mixin 是Dart 2.1 加入的特性，以前版本通常使用abstract class代替。简单来说，mixin是为了解决继承方面的问题而引入的机制，Dart为了支持多重继承，引入了mixin关键字，它最大的特殊处在于：mixin定义的类不能有构造方法，这样可以避免继承多个类而产生的父类构造方法冲突。  
mixins的对象是类，mixins绝不是继承，也不是接口，而是一种全新的特性，可以mixins多个类，mixins的使用需要满足一定条件。  
可以参考Flutter基础知识之Dart语言基础

### 异步编程隔离（Isolate）
Isolate 是 Dart 中进行并发编程的一种方式。由于 Dart 是单线程模型，因此在需要处理 CPU 密集型任务或需要执行长时间运行的操作时，可以使用 Isolate。
- 创建 Isolate  
    在 Dart 中，所有的代码都运行在一个单线程中，这个线程被称为主 Isolate。如果你需要执行耗时的计算，你可以创建一个新的 Isolate，然后在这个新的 Isolate 中执行你的计算。
    ```dart
    import 'dart:isolate';
    
    void printMessage(var message) {
      print('Message: $message');
    }
    
    void main() async {
      var receivePort = ReceivePort();
      await Isolate.spawn(printMessage, 'Hello!', onExit: receivePort.sendPort);
      await for (var message in receivePort) {
        print('Received: $message');
      }
    }
    ```
    在这个示例中，我们使用 Isolate.spawn 创建了一个新的 Isolate。我们传递了一个函数 printMessage 和一个消息 'Hello!' 给这个新的 Isolate。当这个新的 Isolate 完成后，它将使用 onExit 参数指定的 SendPort 发送一个消息。
    需要注意的是，不同的 Isolate 之间不能共享内存，它们只能通过消息传递来进行通信。因此，你不能在一个 Isolate 中访问另一个 Isolate 的变量。
    
- 消息传递  
    在 Dart 中，Isolate 之间的消息传递是通过 SendPort 和 ReceivePort 来实现的。  
    **SendPort和ReceivePort**  
    SendPort 和 ReceivePort 是 Dart 中进行进程间通信的工具。你可以将 SendPort 看作是一个邮箱的地址，ReceivePort 看作是一个邮箱。你可以通过 SendPort 发送消息，然后在对应的 ReceivePort 中接收消息。  
    当你创建一个 ReceivePort 时，它将自动生成一个与之关联的 SendPort：
    ```dart
    var receivePort = ReceivePort();
    var sendPort = receivePort.sendPort;
    ```
    你可以使用 sendPort.send 方法发送消息，然后在 receivePort 中监听消息：  
    ```dart
    sendPort.send('Hello!');
    
    receivePort.listen((message) {
      print('Received: $message');
    });
    ```
- 在Isolate 之间传递消息  
    当你创建一个新的 Isolate 时，你可以将一个 SendPort 传递给这个新的 Isolate。然后你就可以通过这个 SendPort 向新的 Isolate 发送消息，或者从新的 Isolate 接收消息。
    ```dart
    void printMessage(var message) {
      var sendPort = message[0] as SendPort;
      var message = message[1] as String;
      
      print('Message: $message');
    
      sendPort.send('Hello from new Isolate!');
    }
    
    void main() async {
      var receivePort = ReceivePort();
      await Isolate.spawn(printMessage, [receivePort.sendPort, 'Hello!']);
    
      receivePort.listen((message) {
        print('Received: $message');
      });
    }
    ```
    在这个示例中，我们向新的 Isolate 发送了一个列表。这个列表的第一个元素是一个 SendPort，第二个元素是一个字符串。在新的 Isolate 中，我们首先通过 SendPort 发送了一个消息，然后打印了接收到的字符串。在主 Isolate 中，我们监听了 ReceivePort，然后打印了接收到的消息。
需要注意的是，你只能通过 SendPort 发送一些简单的数据，例如数字、字符串、列表、映射等。你不能发送一个函数或者一个对象的实例。

- 应用场景  
    以下列出了一些常见的 Isolate 应用场景：
    - 数据处理  
        对于大量的数据处理或复杂的计算任务，例如图像处理、大文件的读写、大数据集合的排序和筛选等，你可以使用 Isolate 进行处理，防止这些操作阻塞 UI 线程，造成应用程序的卡顿或无响应。
        ```dart
        import 'dart:isolate';
        
        void longRunningTask(SendPort port) {
          // 做一些耗时的操作，例如处理大量数据
          for (int i = 0; i < 1000000000; i++) {}
          port.send("Task done");
        }
        
        void main() {
          var receivePort = ReceivePort();
          Isolate.spawn(longRunningTask, receivePort.sendPort);
          receivePort.listen((message) {
            print(message);
          });
        }
        ```
    - 网络请求  
        尽管 Dart 的 I/O 操作是非阻塞的，但是在进行网络请求并接收数据时，如果数据量较大或需要复杂的处理（如 JSON 或 XML 的解析），这可能会消耗大量的 CPU 时间，从而阻塞 UI 线程。在这种情况下，你可以使用 Isolate。
        ```dart
        void fetchData(SendPort sendPort) async {
          HttpClient httpClient = HttpClient();
          HttpClientRequest request = await httpClient.getUrl(Uri.parse("http://example.com"));
          HttpClientResponse response = await request.close();
          sendPort.send(await consolidateHttpClientResponseBytes(response));
        }
        
        void main() async {
          ReceivePort receivePort = ReceivePort();
          await Isolate.spawn(fetchData, receivePort.sendPort);
          List<int> data = await receivePort.first;
          String result = utf8.decode(data);
          print(result);
        }        
        ```
    - Web 服务器
        在编写 Web 服务器时，你可以使用 Isolate 来处理并发的请求。每当收到新的请求时，你可以创建一个新的 Isolate 来处理请求，这样可以避免阻塞服务器的主线程。
        